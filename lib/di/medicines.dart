import 'package:clinic_management/api/api.dart';
import 'package:clinic_management/api/management_api.dart';
import 'package:clinic_management/model/add_details.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:clinic_management/plain/medicines.dart';

final medicinesDataProvider =
    FutureProvider<ApiResult<List<Medicine>>>((ProviderReference ref) {
  return ManagementApi.medicines();
});

AddableDetails<Medicine, ApiResult?> getAddableMedicineForm() {
  return AddableDetails<Medicine, ApiResult?>(
    Medicine.empty(),
    (value) {
      return ManagementApi.addMedicineInRecord(value);
    },
  );
}
