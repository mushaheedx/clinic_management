import 'package:clinic_management/model/authentication.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final appAuthenticationProvider = FutureProvider<AppAuthentication>(
  (_) async {
    final preferences = await SharedPreferences.getInstance();
    return AppAuthentication(preferences);
  },
);
