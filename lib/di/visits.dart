import 'package:clinic_management/api/api.dart';
import 'package:clinic_management/api/management_api.dart';
import 'package:clinic_management/model/add_details.dart';
import 'package:clinic_management/plain/patient_record.dart';
import 'package:clinic_management/plain/visit_history.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final patientVisitHistoryProvider = FutureProvider.autoDispose
    .family<ApiResult<List<PatientVisitHistoryData>>, PatientRecord>(
        (ref, record) {
  return ManagementApi.patientVisitHistory(record.id ?? '');
});

AddableDetails<PatientVisitHistoryData, ApiResult?>
    getAddableDetailsOfPatientVisit() {
  return AddableDetails<PatientVisitHistoryData, ApiResult?>(
    PatientVisitHistoryData.empty(),
    (value) {
      return ManagementApi.addPatientVisitRecord(value);
    },
  );
}
