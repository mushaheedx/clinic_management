import 'package:clinic_management/model/navigation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final navigationProvider = Provider<AppNavigation>((_) {
  return AppNavigation();
});
