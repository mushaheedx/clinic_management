import 'package:clinic_management/api/api.dart';
import 'package:clinic_management/api/management_api.dart';
import 'package:clinic_management/model/add_details.dart';
import 'package:clinic_management/plain/medical_referer.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final referersDataProvider =
    FutureProvider<ApiResult<List<MedicalRefererRecord>>>(
        (ProviderReference ref) {
  return ManagementApi.medicalRefererRecords();
});

AddableDetails<MedicalRefererRecord, ApiResult?>
    getAddableMedicalReferenceForm() {
  return AddableDetails<MedicalRefererRecord, ApiResult?>(
    MedicalRefererRecord.empty(),
    (value) {
      return ManagementApi.addMedicalReference(value);
    },
  );
}
