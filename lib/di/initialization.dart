import 'package:clinic_management/model/initialization.dart';
import 'package:clinic_management/model/initialization_state.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'navigation.dart';

final appInitializationProvider =
    StateNotifierProvider<AppInitialization, AppInitializationState>(
        (ProviderReference ref) {
  final appNavigation = ref.read(navigationProvider);
  return AppInitialization(ref, appNavigation);
});
