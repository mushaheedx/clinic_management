import 'package:clinic_management/api/api.dart';
import 'package:clinic_management/api/management_api.dart';
import 'package:clinic_management/model/add_details.dart';
import 'package:clinic_management/model/gridview_status.dart';
import 'package:clinic_management/plain/patient_record.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final patientsDataProvider =
    FutureProvider<ApiResult<List<PatientRecord>>>((ProviderReference ref) {
  return ManagementApi.patientsRecords();
});

final patientInfoProvider = FutureProvider.autoDispose
    .family<ApiResult<PatientRecord>, PatientRecord>((ref, record) {
  return ManagementApi.patientRecord(record.id ?? '');
});

final patientGridViewStatusProvider =
    StateNotifierProvider<GridViewStatus, bool>((ref) {
  return GridViewStatus();
});

AddableDetails<PatientRecord, ApiResult?> getAddableDetailsOfPatient() {
  return AddableDetails<PatientRecord, ApiResult?>(PatientRecord.empty(),
      (value) {
    return ManagementApi.addPatientRecord(value);
  });
}
