import 'package:clinic_management/api/fake/medicine.dart';
import 'package:clinic_management/api/fake/sign.dart';
import 'package:clinic_management/plain/autocomplete_form.dart';
import 'package:clinic_management/plain/autocomplete_suggestion.dart';
import 'package:clinic_management/plain/financial_accounts.dart';
import 'package:clinic_management/plain/login_response.dart';
import 'package:clinic_management/plain/medical_referer.dart';
import 'package:clinic_management/plain/medicines.dart';
import 'package:clinic_management/plain/patient_record.dart';
import 'package:clinic_management/plain/visit_history.dart';

import 'api.dart';
import 'fake/patient_record.dart';
import 'fake/referers.dart';
import 'mock.dart';

// ignore: avoid_classes_with_only_static_members
///
/// ```php
/// (
///      [user-agent] => Dart/2.12 (dart:io)
///      [content-type] => application/json; charset=utf-8
///      [accept-encoding] => gzip
///      [content-length] => 41
///      [authorization] => 2626533f25c396f60dbbf8017a9c036f1eb0dc5289ad8479a9cf6a8e83790707
///      [host] => api.empirecinemas.com
///  )
/// ```
class ManagementApi {
  static const appApiAuthority = 'clinic.outsourceowl.in';

  static const dashboardGraphDataMethod = 'admin/GraphData';

  static Future<ApiResult<LoginResponse>> login(
    String username,
    String password,
  ) async {
    final _form = {
      'username': username,
      'passwd': password,
    };

    final _uri = Uri.https(appApiAuthority, '/sign.php');

    final _client = ApiClient<String?>(AppMockClient.mockWeb(fake$Sign));

    final response = await _client.requestJSON(
      _uri,
      body: _form,
      header: {
        'Access-Control-Allow-Origin': '*',
      },
      jsonHeader: false,
      encodeBody: false,
      decodeResponse: false,
    );

    String? _value;

    try {
      _value = response.json as String?;
    } catch (e) {
      //
    }

    return ApiResult<LoginResponse>(
      data: LoginResponse(_value?.trim()),
      error: response.error,
      stackTrace: response.stackTrace,
    );
  }

  static Future<ApiResult<AutocompleteSuggestionsResponse>>
      medicineAutocompleteSuggestions(MedicineAutocompleteSuggestions form) {
    final _uri = Uri.https(
      'www.mims.com',
      '/autocomplete',
      form.toMap,
    );

    final _client = ApiClient<AutocompleteSuggestionsResponse>();

    return _client.request(
      _uri,
      requestType: ApiRequestType.get,
      create: (json) => AutocompleteSuggestionsResponse.fromJson(json),
    );
  }

  static Future<ApiResult<List<PatientRecord>>> patientsRecords() async {
    final _uri = Uri.https(appApiAuthority, '/detailquery.php');
    final _client = ApiClient<List<PatientRecord>>(
        AppMockClient.mockWeb(fake$PatientRecord));
    return _client.request(
      _uri,
      create: (json) => PatientRecord.fromListJson(json),
    );
  }

  static Future<ApiResult<List<MedicalRefererRecord>>>
      medicalRefererRecords() async {
    final _uri = Uri.https(appApiAuthority, '/referencedata.php');
    final _client = ApiClient<List<MedicalRefererRecord>>(
      AppMockClient.mockWeb(fake$MedicineRecord),
    );

    final _body = {
      'type': 'data',
    };

    return _client.request(
      _uri,
      body: _body,
      encodeBody: false,
      jsonHeader: false,
      create: (json) => MedicalRefererRecord.fromListJson(json),
    );
  }

  static Future<ApiResult<PatientRecord>> patientRecord(
      String userOpdId) async {
    final _uri = Uri.https(appApiAuthority, '/form.php');
    final _client = ApiClient<PatientRecord>(
        AppMockClient.mockWeb(fake$SinglePatientRecord));
    final _body = {
      'id': userOpdId,
      'type': 'info',
    };
    return _client.request(
      _uri,
      body: _body,
      encodeBody: false,
      jsonHeader: false,
      create: (json) => PatientRecord.fromJson(json),
    );
  }

  static Future<ApiResult<List<PatientVisitHistoryData>>> patientVisitHistory(
      String userOpdId) async {
    final _uri = Uri.https(appApiAuthority, '/form.php');
    final _client = ApiClient<List<PatientVisitHistoryData>>(
        AppMockClient.mockWeb(fake$SinglePatientRecord));
    final _body = {
      'id': userOpdId,
      'type': 'data',
    };
    return _client.request(
      _uri,
      body: _body,
      encodeBody: false,
      jsonHeader: false,
      create: (json) => PatientVisitHistoryData.fromListJson(json),
    );
  }

  static Future<ApiResult<List<Medicine>>> medicines() async {
    final _uri = Uri.https(appApiAuthority, '/medicinedata.php');
    final _client =
        ApiClient<List<Medicine>>(AppMockClient.mockWeb(fake$medicine));

    final _body = {
      'type': 'data',
    };

    return _client.request(
      _uri,
      body: _body,
      encodeBody: false,
      jsonHeader: false,
      create: (json) => Medicine.fromListJson(json),
    );
  }

  static Future<ApiResult<FinancialAccounts>> getAccounts() {
    final _uri = Uri.https(appApiAuthority, 'acc.php');
    final _client = ApiClient<FinancialAccounts>();

    final _body = {
      'from': '03-05-2021',
      'to': '11-05-2021',
    };

    return _client.request(
      _uri,
      body: _body,
      encodeBody: false,
      jsonHeader: false,
      create: (json) => FinancialAccounts.fromJson(json),
    );
  }

  static Future<ApiResult?> addPatientRecord(PatientRecord value) {
    throw UnimplementedError();
  }

  static Future<ApiResult?> addPatientVisitRecord(
    PatientVisitHistoryData value,
  ) {
    throw UnimplementedError();
  }

  static Future<ApiResult?> addMedicineInRecord(Medicine medicine) {
    throw UnimplementedError();
  }

  static Future<ApiResult?> addMedicalReference(MedicalRefererRecord record) {
    throw UnimplementedError();
  }
}
