import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

bool get _mockEnabled => kDebugMode;

class AppMockClient extends Fake implements http.Client {
  static http.Client? mockAll([String? mockResponse]) {
    if (!_mockEnabled) return null;
    return AppMockClient()..mockResponse(mockResponse ?? '');
  }

  static http.Client? mockWeb(String? mockResponse) {
    if (!_mockEnabled) return null;
    if (kIsWeb) {
      return AppMockClient()..mockResponse(mockResponse ?? '');
    }
  }

  Future<http.Response> _mockResponseBody = Future.value(
    http.Response('', 400),
  );

  void mockResponse(String responseBody) {
    _mockResponseBody = Future.value(http.Response(responseBody, 200));
  }

  @override
  Future<http.Response> get(Uri url, {Map<String, String>? headers}) {
    Fimber.w('Replying with FAKE [Response]');
    return _mockResponseBody;
  }

  @override
  Future<http.Response> post(Uri url,
      {Map<String, String>? headers, Object? body, Encoding? encoding}) {
    Fimber.w('Replying with FAKE [Response]');
    return _mockResponseBody;
  }
}
