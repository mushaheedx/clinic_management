import 'dart:convert';

final fake$medicine = json.encode(_map);

final _map = [
  {
    "ID": "37",
    "MedicineName": "2 uz",
    "IsActive": "1",
    "Date": "26-04-2021 13:36:21"
  },
  {
    "ID": "36",
    "MedicineName": "NA",
    "IsActive": "1",
    "Date": "26-04-2021 06:54:58"
  },
  {
    "ID": "35",
    "MedicineName": "Syrup",
    "IsActive": "1",
    "Date": "26-04-2021 05:53:31"
  },
  {
    "ID": "34",
    "MedicineName": "MxCV-625",
    "IsActive": "1",
    "Date": "26-04-2021 05:52:37"
  },
  {
    "ID": "33",
    "MedicineName": "Mox250DT",
    "IsActive": "1",
    "Date": "26-04-2021 05:52:19"
  },
  {
    "ID": "32",
    "MedicineName": "Mox500",
    "IsActive": "1",
    "Date": "26-04-2021 05:52:09"
  },
  {
    "ID": "31",
    "MedicineName": "Kofex",
    "IsActive": "1",
    "Date": "26-04-2021 05:51:08"
  },
  {
    "ID": "30",
    "MedicineName": "Doxy",
    "IsActive": "1",
    "Date": "26-04-2021 05:51:01"
  },
  {
    "ID": "29",
    "MedicineName": "Ibu",
    "IsActive": "1",
    "Date": "26-04-2021 05:50:54"
  },
  {
    "ID": "28",
    "MedicineName": "Cetri-gr",
    "IsActive": "1",
    "Date": "26-04-2021 05:50:44"
  },
  {
    "ID": "27",
    "MedicineName": "Cetri-yell",
    "IsActive": "1",
    "Date": "26-04-2021 05:50:36"
  },
  {
    "ID": "26",
    "MedicineName": "A&D",
    "IsActive": "1",
    "Date": "26-04-2021 05:50:06"
  },
  {
    "ID": "25",
    "MedicineName": "Folic",
    "IsActive": "1",
    "Date": "26-04-2021 05:49:59"
  },
  {
    "ID": "24",
    "MedicineName": "Bplex",
    "IsActive": "1",
    "Date": "26-04-2021 05:49:51"
  },
  {
    "ID": "23",
    "MedicineName": "Diclo-SP",
    "IsActive": "1",
    "Date": "26-04-2021 05:49:36"
  },
  {
    "ID": "22",
    "MedicineName": "Diclo",
    "IsActive": "1",
    "Date": "26-04-2021 05:49:18"
  },
  {
    "ID": "21",
    "MedicineName": "Furo",
    "IsActive": "1",
    "Date": "26-04-2021 05:49:06"
  },
  {
    "ID": "20",
    "MedicineName": "Metro",
    "IsActive": "1",
    "Date": "26-04-2021 05:49:00"
  },
  {
    "ID": "19",
    "MedicineName": "Cyclo",
    "IsActive": "1",
    "Date": "26-04-2021 05:48:53"
  },
  {
    "ID": "18",
    "MedicineName": "Dom",
    "IsActive": "1",
    "Date": "26-04-2021 05:48:37"
  },
  {
    "ID": "17",
    "MedicineName": "UZ",
    "IsActive": "1",
    "Date": "26-04-2021 05:48:31"
  },
  {
    "ID": "16",
    "MedicineName": "Famo-40",
    "IsActive": "1",
    "Date": "26-04-2021 05:48:21"
  },
  {
    "ID": "15",
    "MedicineName": "Famo-20",
    "IsActive": "1",
    "Date": "26-04-2021 05:48:13"
  },
  {
    "ID": "14",
    "MedicineName": "CPM",
    "IsActive": "1",
    "Date": "26-04-2021 05:47:41"
  },
  {
    "ID": "13",
    "MedicineName": "Dexa",
    "IsActive": "1",
    "Date": "26-04-2021 05:47:24"
  },
  {
    "ID": "12",
    "MedicineName": "Deri",
    "IsActive": "1",
    "Date": "26-04-2021 05:47:16"
  },
  {
    "ID": "11",
    "MedicineName": "DCC",
    "IsActive": "1",
    "Date": "26-04-2021 05:47:06"
  },
  {
    "ID": "10",
    "MedicineName": "Ni- half",
    "IsActive": "1",
    "Date": "26-04-2021 05:46:49"
  },
  {
    "ID": "9",
    "MedicineName": "Ni",
    "IsActive": "1",
    "Date": "26-04-2021 05:46:07"
  },
  {
    "ID": "8",
    "MedicineName": "PN",
    "IsActive": "1",
    "Date": "26-04-2021 05:45:33"
  },
  {
    "ID": "7",
    "MedicineName": "CP-325 gr",
    "IsActive": "1",
    "Date": "26-04-2021 05:45:19"
  },
  {
    "ID": "6",
    "MedicineName": "CP-325 pk",
    "IsActive": "1",
    "Date": "26-04-2021 05:45:08"
  },
  {
    "ID": "5",
    "MedicineName": "CP-325 yell",
    "IsActive": "1",
    "Date": "26-04-2021 05:44:51"
  },
  {
    "ID": "4",
    "MedicineName": "CP-325 blu",
    "IsActive": "1",
    "Date": "26-04-2021 05:42:26"
  },
  {
    "ID": "3",
    "MedicineName": "CP-500 pk",
    "IsActive": "1",
    "Date": "26-04-2021 05:41:57"
  },
  {
    "ID": "2",
    "MedicineName": "Para-O",
    "IsActive": "1",
    "Date": "26-04-2021 05:41:23"
  },
  {
    "ID": "1",
    "MedicineName": "Para-R",
    "IsActive": "1",
    "Date": "26-04-2021 05:41:03"
  }
];
