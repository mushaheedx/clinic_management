import 'dart:convert';

final fake$MedicineRecord = json.encode(_map);

final _map = [
  {
    "ID": "11",
    "Reference": "AR Hospital, Malad",
    "Type": "Hospital",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "10",
    "Reference": "Balajee Hospital IPD",
    "Type": "Hospital",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "9",
    "Reference": "ENT - Dr. Nilesh Choradia",
    "Type": "Consultant Doctor",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "8",
    "Reference": "Dr. Nilesh Choradia",
    "Type": "Consultant Doctor",
    "IsActive": "0",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "7",
    "Reference": "NA",
    "Type": "Hospital",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "6",
    "Reference": "Shobha- Xray/USG",
    "Type": "Diagnostic Center",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "5",
    "Reference": "Muskan(M.N.)- Xray/USG",
    "Type": "Diagnostic Center",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "4",
    "Reference": "Shubh- USG",
    "Type": "Diagnostic Center",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "3",
    "Reference": "Shubh- Xray",
    "Type": "Diagnostic Center",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "2",
    "Reference": "Thakkar Lab",
    "Type": "Pathology",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  },
  {
    "ID": "1",
    "Reference": "Shameem Lab",
    "Type": "Pathology",
    "IsActive": "1",
    "Date": "0000-00-00 00:00:00"
  }
];
