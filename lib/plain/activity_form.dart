import 'package:intl/intl.dart' as intl;

import 'form.dart';

class AppActivityRequestForm extends AppForm {
  // ignore: avoid_redundant_argument_values
  final initialDateTime = DateTime(2021, 1, 1);

  DateTime? _lastActiveSinceDateTime;

  DateTime get lastActiveSinceDateTime =>
      _lastActiveSinceDateTime ?? initialDateTime;

  void changeDate(DateTime? date) {
    if (date == null) return;
    _lastActiveSinceDateTime = date;
  }

  final intl.DateFormat formatter = intl.DateFormat('dd/MM/yyyy');

  String get lastActiveUsersSince {
    return formatter.format(lastActiveSinceDateTime);
  }

  String get now {
    return formatter.format(DateTime.now());
  }

  static String toUnixTimestampInSeconds(DateTime dateTime) {
    final _dateTime = DateTime(dateTime.year, dateTime.month, dateTime.day);
    // if (dateTime == null) return "1469392779";
    final _time = _dateTime.millisecondsSinceEpoch / 1000;
    return '${_time.round()}';
  }

  @override
  Map<String, dynamic> get toMap {
    return {
      // "extra_fields": ["country", "location"],
      "last_active_since": toUnixTimestampInSeconds(lastActiveSinceDateTime),
      "segment_name": "Active Users"
    };
  }
}
