// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'autocomplete_suggestion.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AutocompleteSuggestion _$AutocompleteSuggestionFromJson(
    Map<String, dynamic> json) {
  return AutocompleteSuggestion(
    json['value'] as String?,
  );
}

Map<String, dynamic> _$AutocompleteSuggestionToJson(
        AutocompleteSuggestion instance) =>
    <String, dynamic>{
      'value': instance.value,
    };

AutocompleteSuggestionsResponse _$AutocompleteSuggestionsResponseFromJson(
    Map<String, dynamic> json) {
  return AutocompleteSuggestionsResponse(
    json['query'] as String?,
    (json['suggestions'] as List<dynamic>)
        .map((e) => e == null ? null : AutocompleteSuggestion.fromJson(e))
        .toList(),
  );
}

Map<String, dynamic> _$AutocompleteSuggestionsResponseToJson(
        AutocompleteSuggestionsResponse instance) =>
    <String, dynamic>{
      'query': instance.query,
      'suggestions': instance.suggestions,
    };
