import 'form.dart';

class LoginForm implements AppForm {
  String _username = '';

  String get username => _username;

  void saveUsername(String? value) {
    _username = value ?? '';
  }

  String _password = '';

  String get password => _password;

  void savePassword(String? value) {
    _password = value ?? '';
  }

  @override
  Map<String, String> get toMap {
    return {
      'UserName': username,
      'Password': password,
      'grant_type': 'password',
    };
  }
}
