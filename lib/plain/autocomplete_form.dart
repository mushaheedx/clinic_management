import 'form.dart';

class MedicineAutocompleteSuggestions extends AppForm {
  String countryCode = 'IN';
  String query = '';

  MedicineAutocompleteSuggestions(this.query);

  bool get doQuery => query.isNotEmpty;

  @override
  Map<String, dynamic> get toMap => {
        'countryCode': countryCode,
        'query': query,
      };
}
