// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicines.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Medicine _$MedicineFromJson(Map<String, dynamic> json) {
  return Medicine(
    json['ID'] as String?,
    json['MedicineName'] as String?,
    json['IsActive'] as String?,
    json['Date'] as String?,
  );
}

Map<String, dynamic> _$MedicineToJson(Medicine instance) => <String, dynamic>{
      'ID': instance.id,
      'MedicineName': instance.medicineName,
      'IsActive': instance.isActive,
      'Date': instance.date,
    };
