import 'package:json_annotation/json_annotation.dart';

part 'medicines.g.dart';

@JsonSerializable()
class Medicine with Comparable<Medicine?> {
  @JsonKey(name: 'ID')
  final String? id;

  @JsonKey(name: 'MedicineName')
  final String? medicineName;

  @JsonKey(name: 'IsActive')
  final String? isActive;

  // example format: 26-04-2021 13:36:21
  @JsonKey(name: 'Date')
  final String? date;

  Medicine(this.id, this.medicineName, this.isActive, this.date);

  Medicine.empty({
    this.id,
    this.medicineName,
    this.isActive,
    this.date,
  });

  Medicine copyWith({
    String? id,
    String? medicineName,
    String? isActive,
    String? date,
  }) {
    return Medicine(
      id ?? this.id,
      medicineName ?? this.medicineName,
      isActive ?? this.isActive,
      date ?? this.date,
    );
  }

  factory Medicine.fromJson(dynamic json) =>
      _$MedicineFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$MedicineToJson(this)
    ..removeWhere(
      (key, value) => value == null,
    );

  static List<Medicine> fromListJson(dynamic json) {
    final _iterableJson = json as Iterable;
    return _iterableJson.map<Medicine>((e) => Medicine.fromJson(e)).toList();
  }

  @override
  int compareTo(Medicine? other) {
    if (other?.date == null) return 1;
    final _value = date?.compareTo(other?.date ?? '') ?? 1;
    return _value * -1;
  }
}
