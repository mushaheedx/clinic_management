import 'package:json_annotation/json_annotation.dart';

part 'medical_referer.g.dart';

enum ReferenceType {
  // ignore: constant_identifier_names
  Hospital,
  // ignore: constant_identifier_names
  Consultant_Doctor,
  // ignore: constant_identifier_names
  Pathology,
  // ignore: constant_identifier_names
  Diagnostic_Center,
}

@JsonSerializable()
class MedicalRefererRecord with Comparable<MedicalRefererRecord?> {
  @JsonKey(name: 'ID')
  final String? id;

  @JsonKey(name: 'Reference')
  final String? reference;

  @JsonKey(name: 'Type')
  final String? referenceType;

  @JsonKey(name: 'IsActive')
  final String? isActive;

  // example format: 2021-04-26 13:36:21
  @JsonKey(name: 'Date')
  final String? date;

  MedicalRefererRecord(
    this.id,
    this.reference,
    this.referenceType,
    this.isActive,
    this.date,
  );

  MedicalRefererRecord.empty({
    this.id,
    this.reference,
    this.referenceType,
    this.isActive,
    this.date,
  });

  MedicalRefererRecord copyWith({
    String? id,
    String? reference,
    String? referenceType,
    String? isActive,
    String? date,
  }) {
    return MedicalRefererRecord(
      id ?? this.id,
      reference ?? this.reference,
      referenceType ?? this.referenceType,
      isActive ?? this.isActive,
      date ?? this.date,
    );
  }

  Map<String, dynamic> toJson() => _$MedicalRefererRecordToJson(this);

  factory MedicalRefererRecord.fromJson(dynamic json) =>
      _$MedicalRefererRecordFromJson(json as Map<String, dynamic>);

  static List<MedicalRefererRecord> fromListJson(dynamic json) {
    final _iterableJson = json as Iterable;
    return _iterableJson
        .map<MedicalRefererRecord>((e) => MedicalRefererRecord.fromJson(e))
        .toList();
  }

  @override
  int compareTo(MedicalRefererRecord? other) {
    if (other?.date == null) return 1;
    final _value = date?.compareTo(other?.date ?? '') ?? 1;
    return _value * -1;
  }
}
