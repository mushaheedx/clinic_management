import 'package:json_annotation/json_annotation.dart';

part 'autocomplete_suggestion.g.dart';

@JsonSerializable()
class AutocompleteSuggestion {
  @JsonKey(name: 'value')
  final String? value;

  AutocompleteSuggestion(this.value);

  factory AutocompleteSuggestion.fromJson(dynamic json) =>
      _$AutocompleteSuggestionFromJson(json as Map<String, dynamic>);
}

@JsonSerializable()
class AutocompleteSuggestionsResponse {
  @JsonKey(name: 'query')
  final String? query;
  @JsonKey(name: 'suggestions')
  final List<AutocompleteSuggestion?> suggestions;

  AutocompleteSuggestionsResponse(
    this.query,
    this.suggestions,
  );

  factory AutocompleteSuggestionsResponse.fromJson(dynamic json) =>
      _$AutocompleteSuggestionsResponseFromJson(json as Map<String, dynamic>);
}
