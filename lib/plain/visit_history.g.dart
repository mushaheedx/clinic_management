// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'visit_history.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientVisitHistoryData _$PatientVisitHistoryDataFromJson(
    Map<String, dynamic> json) {
  return PatientVisitHistoryData(
    json['id'] as String?,
    json['datevisit'] as String?,
    json['date'] as String?,
    json['additional'] as String?,
    json['fee'] as String?,
    json['balance'] as String?,
    json['complaint'] as String?,
    json['follow'] as String?,
    json['medicine'] as String?,
    json['reference'] as String?,
    json['time'] as String?,
    json['treatment'] as String?,
    json['uid'] as String?,
  );
}

Map<String, dynamic> _$PatientVisitHistoryDataToJson(
        PatientVisitHistoryData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'datevisit': instance.dateVisit,
      'date': instance.date,
      'additional': instance.additionalRemarks,
      'fee': instance.fee,
      'balance': instance.feeBalance,
      'complaint': instance.complaint,
      'follow': instance.followUpDetails,
      'medicine': instance.medicine,
      'reference': instance.reference,
      'time': instance.time,
      'treatment': instance.treatment,
      'uid': instance.uid,
    };
