class LoginResponse {
  final String? response;

  LoginResponse(this.response);

  bool get isSuccess => response?.toLowerCase() == 'success';
}
