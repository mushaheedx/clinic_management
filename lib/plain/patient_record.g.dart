// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'patient_record.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientRecord _$PatientRecordFromJson(Map<String, dynamic> json) {
  return PatientRecord(
    json['id'] as String?,
    json['opd'] as String?,
    json['name'] as String?,
    json['age'] as String?,
    json['gender'] as String?,
    json['address'] as String?,
    json['mobile'] as String?,
    json['datevisit'] as String?,
    json['IsActive'] as String?,
    json['date'] as String?,
  );
}

Map<String, dynamic> _$PatientRecordToJson(PatientRecord instance) =>
    <String, dynamic>{
      'id': instance.id,
      'opd': instance.opdId,
      'name': instance.name,
      'age': instance.age,
      'gender': instance.gender,
      'address': instance.address,
      'mobile': instance.mobile,
      'datevisit': instance.dateVisit,
      'IsActive': instance.isActive,
      'date': instance.date,
    };
