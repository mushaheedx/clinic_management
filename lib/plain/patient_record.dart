import 'package:json_annotation/json_annotation.dart';

part 'patient_record.g.dart';

@JsonSerializable()
class PatientRecord with Comparable<PatientRecord?> {
  @JsonKey(name: 'id')
  final String? id;
  @JsonKey(name: 'opd')
  final String? opdId;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'age')
  final String? age;
  @JsonKey(name: 'gender')
  final String? gender;
  @JsonKey(name: 'address')
  final String? address;
  @JsonKey(name: 'mobile')
  final String? mobile;
  @JsonKey(name: 'datevisit')
  final String? dateVisit;
  @JsonKey(name: 'IsActive')
  final String? isActive;
  @JsonKey(name: 'date')
  final String? date;

  PatientRecord(
    this.id,
    this.opdId,
    this.name,
    this.age,
    this.gender,
    this.address,
    this.mobile,
    this.dateVisit,
    this.isActive,
    this.date,
  );

  PatientRecord.empty({
    this.id,
    this.opdId,
    this.name,
    this.age,
    this.gender,
    this.address,
    this.mobile,
    this.dateVisit,
    this.isActive,
    this.date,
  });

  PatientRecord copyWith({
    String? id,
    String? opdId,
    String? name,
    String? age,
    String? gender,
    String? address,
    String? mobile,
    String? dateVisit,
    String? isActive,
    String? date,
  }) =>
      PatientRecord(
        id ?? this.id,
        opdId ?? this.opdId,
        name ?? this.name,
        age ?? this.age,
        gender ?? this.gender,
        address ?? this.address,
        mobile ?? this.mobile,
        dateVisit ?? this.dateVisit,
        isActive ?? this.isActive,
        date ?? this.date,
      );

  factory PatientRecord.fromJson(dynamic json) =>
      _$PatientRecordFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PatientRecordToJson(this);

  static List<PatientRecord> fromListJson(dynamic json) {
    final _iterableJson = json as Iterable;
    return _iterableJson.map<PatientRecord>((e) {
      if (e == null) return PatientRecord.empty();
      return PatientRecord.fromJson(e);
    }).toList();
  }

  @override
  int compareTo(PatientRecord? other) {
    if (other?.date == null) return 1;
    final _value = date?.compareTo(other?.date ?? '') ?? 1;
    return _value * -1;
  }
}
