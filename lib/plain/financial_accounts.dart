import 'package:json_annotation/json_annotation.dart';

part 'financial_accounts.g.dart';

@JsonSerializable()
class FinancialAccountData {
  @JsonKey(name: 'datevisit')
  final String dateVisit;
  @JsonKey(name: 'Fees')
  final String fees;

  FinancialAccountData(
    this.dateVisit,
    this.fees,
  );

  factory FinancialAccountData.fromJson(dynamic json) =>
      _$FinancialAccountDataFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$FinancialAccountDataToJson(this);
}

@JsonSerializable()
class FinancialAccounts {
  @JsonKey(name: 'acc')
  final List<FinancialAccountData> accounts;

  FinancialAccounts(this.accounts);

  factory FinancialAccounts.fromJson(dynamic json) =>
      _$FinancialAccountsFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$FinancialAccountsToJson(this);
}
