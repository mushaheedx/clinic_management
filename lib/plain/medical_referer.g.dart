// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medical_referer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicalRefererRecord _$MedicalRefererRecordFromJson(Map<String, dynamic> json) {
  return MedicalRefererRecord(
    json['ID'] as String?,
    json['Reference'] as String?,
    json['Type'] as String?,
    json['IsActive'] as String?,
    json['Date'] as String?,
  );
}

Map<String, dynamic> _$MedicalRefererRecordToJson(
        MedicalRefererRecord instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'Reference': instance.reference,
      'Type': instance.referenceType,
      'IsActive': instance.isActive,
      'Date': instance.date,
    };
