// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'financial_accounts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FinancialAccountData _$FinancialAccountDataFromJson(Map<String, dynamic> json) {
  return FinancialAccountData(
    json['datevisit'] as String,
    json['Fees'] as String,
  );
}

Map<String, dynamic> _$FinancialAccountDataToJson(
        FinancialAccountData instance) =>
    <String, dynamic>{
      'datevisit': instance.dateVisit,
      'Fees': instance.fees,
    };

FinancialAccounts _$FinancialAccountsFromJson(Map<String, dynamic> json) {
  return FinancialAccounts(
    (json['acc'] as List<dynamic>)
        .map((e) => FinancialAccountData.fromJson(e))
        .toList(),
  );
}

Map<String, dynamic> _$FinancialAccountsToJson(FinancialAccounts instance) =>
    <String, dynamic>{
      'acc': instance.accounts,
    };
