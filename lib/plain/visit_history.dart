import 'package:json_annotation/json_annotation.dart';

part 'visit_history.g.dart';

@JsonSerializable()
class PatientVisitHistoryData with Comparable<PatientVisitHistoryData?> {
  @JsonKey(name: 'id')
  final String? id;
  @JsonKey(name: 'datevisit')
  final String? dateVisit;
  @JsonKey(name: 'date')
  final String? date;
  @JsonKey(name: 'additional')
  final String? additionalRemarks;
  @JsonKey(name: 'fee')
  final String? fee;
  @JsonKey(name: 'balance')
  final String? feeBalance;
  @JsonKey(name: 'complaint')
  final String? complaint;
  @JsonKey(name: 'follow')
  final String? followUpDetails;
  @JsonKey(name: 'medicine')
  final String? medicine;
  @JsonKey(name: 'reference')
  final String? reference;
  @JsonKey(name: 'time')
  final String? time;
  @JsonKey(name: 'treatment')
  final String? treatment;
  @JsonKey(name: 'uid')
  final String? uid;

  PatientVisitHistoryData(
    this.id,
    this.dateVisit,
    this.date,
    this.additionalRemarks,
    this.fee,
    this.feeBalance,
    this.complaint,
    this.followUpDetails,
    this.medicine,
    this.reference,
    this.time,
    this.treatment,
    this.uid,
  );

  PatientVisitHistoryData.empty({
    this.id,
    this.dateVisit,
    this.date,
    this.additionalRemarks,
    this.fee,
    this.feeBalance,
    this.complaint,
    this.followUpDetails,
    this.medicine,
    this.reference,
    this.time,
    this.treatment,
    this.uid,
  });

  PatientVisitHistoryData copyWith({
    String? id,
    String? dateVisit,
    String? date,
    String? additionalRemarks,
    String? fee,
    String? feeBalance,
    String? complaint,
    String? followUpDetails,
    String? medicine,
    String? reference,
    String? time,
    String? treatment,
    String? uid,
  }) =>
      PatientVisitHistoryData(
        id ?? this.id,
        dateVisit ?? this.dateVisit,
        date ?? this.date,
        additionalRemarks ?? this.additionalRemarks,
        fee ?? this.fee,
        feeBalance ?? this.feeBalance,
        complaint ?? this.complaint,
        followUpDetails ?? this.followUpDetails,
        medicine ?? this.medicine,
        reference ?? this.reference,
        time ?? this.time,
        treatment ?? this.treatment,
        uid ?? this.uid,
      );

  factory PatientVisitHistoryData.fromJson(dynamic json) =>
      _$PatientVisitHistoryDataFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PatientVisitHistoryDataToJson(this)
    ..removeWhere((key, value) => value == null);

  static List<PatientVisitHistoryData> fromListJson(dynamic json) {
    final _iterableJson = json as Iterable;
    return _iterableJson.map<PatientVisitHistoryData>((e) {
      if (e == null) return PatientVisitHistoryData.empty();
      return PatientVisitHistoryData.fromJson(e);
    }).toList();
  }

  @override
  int compareTo(PatientVisitHistoryData? other) {
    if (other?.date == null) return 1;
    final _value = date?.compareTo(other?.date ?? '') ?? 1;
    return _value * -1;
  }
}
