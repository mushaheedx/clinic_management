import 'package:flutter/widgets.dart';

class AppAsset {
  static const appLogo = 'images/logo.v1.png';
}

class AppImageAsset {
  static const appLogo = AssetImage(AppAsset.appLogo);
}
