class AppText {
  static const applicationTitle = 'Clinic Management';
  static const dashboard = 'dashboard';
  static const login = 'Login';

  static const signInToYourAccount = 'Sign in to your accont';

  static const somethingWentWrong = 'Something went wrong';

  static const loginFailed = 'Login failed. $somethingWentWrong';
}

class HeroTags {
  static const accountsIcon = '@AccountsIcon';
}
