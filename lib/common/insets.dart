import 'package:flutter/painting.dart';

class Dimension {
  static const keyline0 = 0.0;
  static const keyline1 = 2.0;
  static const keyline2 = 4.0;
  static const keyline3 = 6.0;
  static const keyline4 = 8.0;
  static const keyline5 = 10.0;
  static const keyline6 = 12.0;
  static const keyline7 = 14.0;
  static const keyline8 = 16.0;
  static const keyline9 = 18.0;
  static const keyline10 = 20.0;
}

class AppInsets {
  static const all8 = EdgeInsets.all(Dimension.keyline4);
  static const all12 = EdgeInsets.all(Dimension.keyline6);
  static const all16 = EdgeInsets.all(Dimension.keyline8);
}
