class AppRouteName {
  static const root = '/';
  static const dashboard = '/dashboard';
  static const login = '/login';

  static const patientRecords = '$dashboard/patient';
  static const patientRecordsAdd = '$patientRecords/add';
  static const patientInfo = '$patientRecords/info';
  static const patientVisitHistory = '$patientInfo/visit_history';
  static const patientAddVisit = '$patientVisitHistory/add';

  static const medicineRecords = '$dashboard/medicine';
  static const medicineRecordsAdd = '$medicineRecords/add';

  static const medicalRefererRecords = '$dashboard/referer';
  static const medicalRefererRecordsAdd = '$medicalRefererRecords/add';

  static const accountsScreen = '/accounts';
}
