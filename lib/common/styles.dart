import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppDecoration {
  static const LinearGradient gradient = LinearGradient(
    colors: [
      Color(0xFFc9d6ff),
      Color(0xFFe2e2e2),
    ],
    stops: [
      0,
      1,
    ],
  );

  static const LinearGradient gradientBackground = LinearGradient(
    colors: [
      Color(0x66b2c4fe),
      Color(0x66d6d6d6),
    ],
    stops: [
      0,
      1,
    ],
  );

  static const cardShapeBorder = RoundedRectangleBorder(
    borderRadius: cardBorderRadius,
  );

  static const cardBorderRadius = BorderRadius.all(Radius.circular(12.0));

  static const emptyBox = SizedBox();
}

// ignore: avoid_classes_with_only_static_members
class AppTheme {
  static const MaterialColor primarySwatch = Colors.deepPurple;
  static const AlwaysStoppedAnimation<Color> progressValueColor =
      AlwaysStoppedAnimation<Color>(AppTheme.primarySwatch);
  static final textTheme = TextTheme(
    headline1: GoogleFonts.josefinSans(
        fontSize: 124, fontWeight: FontWeight.w300, letterSpacing: -1.5),
    headline2: GoogleFonts.josefinSans(
        fontSize: 77, fontWeight: FontWeight.w300, letterSpacing: -0.5),
    headline3:
        GoogleFonts.josefinSans(fontSize: 62, fontWeight: FontWeight.w400),
    headline4: GoogleFonts.josefinSans(
        fontSize: 44, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    headline5:
        GoogleFonts.josefinSans(fontSize: 31, fontWeight: FontWeight.w400),
    headline6: GoogleFonts.josefinSans(
        fontSize: 26, fontWeight: FontWeight.w500, letterSpacing: 0.15),
    subtitle1: GoogleFonts.josefinSans(
        fontSize: 21, fontWeight: FontWeight.w400, letterSpacing: 0.15),
    subtitle2: GoogleFonts.josefinSans(
        fontSize: 18, fontWeight: FontWeight.w500, letterSpacing: 0.1),
    bodyText1: GoogleFonts.roboto(
        fontSize: 16, fontWeight: FontWeight.w400, letterSpacing: 0.5),
    bodyText2: GoogleFonts.roboto(
        fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    button: GoogleFonts.roboto(
        fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 1.25),
    caption: GoogleFonts.roboto(
        fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
    overline: GoogleFonts.roboto(
        fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
  );
}
