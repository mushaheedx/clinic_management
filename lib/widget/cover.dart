import 'dart:ui' as ui;
import 'package:flutter/material.dart';

const appScaffoldCoverColor = Colors.white10;

class ScaffoldCover extends StatelessWidget {
  final Widget child;

  const ScaffoldCover({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/blob.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: BackdropFilter(
        filter: ui.ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        child: child,
      ),
    );
  }
}
