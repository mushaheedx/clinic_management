import 'package:clinic_management/utils/date_time.dart';
import 'package:clinic_management/utils/enum.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart' as intl;

const _borderRadius = BorderRadius.all(Radius.circular(8.0));

class TextFormFieldTools {
  final GlobalKey<FormFieldState<String>>? _globalKey;
  late TextEditingController controller;
  late FocusNode focusNode;

  TextFormFieldTools([bool createGlobalKey = false])
      : _globalKey =
            createGlobalKey ? GlobalKey<FormFieldState<String>>() : null;

  void onInitState() {
    controller = TextEditingController();
    focusNode = FocusNode();
  }

  void insertTextWithinComposingRange(TextRange composingRange, String value) {
    if (!composingRange.isValid) {
      return;
    }
    final _text = controller.text.replaceRange(
      composingRange.start,
      composingRange.end,
      value,
    );

    replaceAllText(_text);
  }

  void replaceAllText(String value) {
    if (_globalKey != null) {
      _globalKey!.currentState?.didChange(value);
    }
    // DidChange didn't notify controller
    controller.text = value;
  }

  void onDispose() {
    controller.dispose();
    focusNode.dispose();
  }
}

class AppLoginFormField extends StatelessWidget {
  final IconData icon;
  final String hintText;
  final FormFieldSetter<String> onSave;
  final bool obscureText;
  final TextInputType? keyboardType;
  final Iterable<String>? autofillHints;

  const AppLoginFormField({
    Key? key,
    required this.icon,
    required this.hintText,
    required this.onSave,
    this.obscureText = false,
    this.keyboardType,
    this.autofillHints,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onSaved: onSave,
      obscureText: obscureText,
      keyboardType: keyboardType,
      autofillHints: autofillHints,
      cursorColor: Colors.white,
      decoration: InputDecoration(
        hintText: hintText,
        prefixIcon: Icon(
          icon,
        ),
        border: const OutlineInputBorder(
          borderRadius: _borderRadius,
        ),
      ),
    );
  }
}

class AppTextFormField extends StatefulWidget {
  final TextFormFieldTools? tools;
  final String labelText;
  final String? hintText;
  final FormFieldSetter<String> onSave;
  final FormFieldValidator<String>? onValidate;
  final bool obscureText;
  final TextInputType? keyboardType;
  final Iterable<String>? autofillHints;
  final int? maxLines;
  final Widget? suffixIcon;
  final ValueChanged<String>? onFieldSubmitted;
  final VoidCallback? onEditingComplete;

  const AppTextFormField({
    Key? key,
    required this.labelText,
    required this.onSave,
    required this.onValidate,
    this.tools,
    this.obscureText = false,
    this.maxLines = 1,
    this.hintText,
    this.keyboardType,
    this.autofillHints,
    this.suffixIcon,
    this.onFieldSubmitted,
    this.onEditingComplete,
  }) : super(key: key);

  @override
  _AppTextFormFieldState createState() => _AppTextFormFieldState();
}

class _AppTextFormFieldState extends State<AppTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: widget.tools?._globalKey,
      controller: widget.tools?.controller,
      focusNode: widget.tools?.focusNode,
      maxLines: widget.maxLines,
      decoration: InputDecoration(
        labelText: widget.labelText,
        hintText: widget.hintText,
        suffixIcon: widget.suffixIcon,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        border: const OutlineInputBorder(),
      ),
      keyboardType: widget.keyboardType,
      validator: widget.onValidate,
      onSaved: widget.onSave,
      onEditingComplete: widget.onEditingComplete,
      onFieldSubmitted: widget.onFieldSubmitted,
    );
  }
}

class AppDateFormField extends StatefulWidget {
  final IconData icon;
  final String hintText;
  final void Function(String?, DateTime?) onSave;
  final void Function(TextEditingController)? onController;
  final String? Function(String?, DateTime?)? onValidate;
  final bool obscureText;
  final TextInputType? keyboardType;
  final Iterable<String>? autofillHints;
  final String label;
  final Color labelColor;
  final DateTime? initialDate;
  final DateTime? lastDate;
  final bool useInvertedTheme;

  const AppDateFormField({
    Key? key,
    required this.icon,
    required this.hintText,
    required this.onSave,
    required this.label,
    this.useInvertedTheme = false,
    this.onValidate,
    this.onController,
    this.obscureText = false,
    this.keyboardType,
    this.autofillHints,
    this.labelColor = Colors.black,
    this.initialDate,
    this.lastDate,
  }) : super(key: key);

  @override
  _AppDateFormFieldState createState() => _AppDateFormFieldState();
}

class _AppDateFormFieldState extends State<AppDateFormField> {
  late TextEditingController controller;

  final intl.DateFormat formatter = intl.DateFormat('dd/MM/yyyy');

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
    if (widget.initialDate != null) {
      controller.text = formatter.format(widget.initialDate!);
    }
    if (widget.onController != null) widget.onController!(controller);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  DateTime dateTime = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    dateTime = await showDatePicker(
          context: context,
          initialDate: widget.initialDate ?? DateTime.now(),
          firstDate: DateTime(2018),
          lastDate: widget.lastDate ?? DateTime.now(),
        ) ??
        DateTime.now();

    final String formatted = formatter.format(dateTime);

    setState(() {
      controller.text = formatted;
    });
  }

  void _onSave(String? value) {
    widget.onSave(value, dateTime);
  }

  String? _validator(String? value) {
    if (widget.onValidate != null) {
      widget.onValidate!(value, dateTime);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.useInvertedTheme) {
      return TextFormField(
        controller: controller,
        obscureText: widget.obscureText,
        keyboardType: TextInputType.datetime,
        autofillHints: widget.autofillHints,
        readOnly: true,
        decoration: InputDecoration(
          hintText: widget.hintText,
          labelText: widget.label,
          suffixIcon: Icon(widget.icon),
          border: const OutlineInputBorder(),
        ),
        onTap: () => _selectDate(context),
        onSaved: _onSave,
        validator: _validator,
      );
    }

    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;
    final bool isThemeDark = theme.brightness == Brightness.dark;
    final Color buttonColor =
        isThemeDark ? colorScheme.primaryVariant : colorScheme.secondary;

    final Brightness brightness =
        isThemeDark ? Brightness.light : Brightness.dark;
    final Color themeBackgroundColor = isThemeDark
        ? colorScheme.onSurface
        : Color.alphaBlend(
            colorScheme.onSurface.withOpacity(0.80), colorScheme.surface);

    final ThemeData inverseTheme = theme.copyWith(
      colorScheme: ColorScheme(
        primary: colorScheme.onPrimary,
        primaryVariant: colorScheme.onPrimary,
        // For the button color, the spec says it should be primaryVariant, but for
        // backward compatibility on light themes we are leaving it as secondary.
        secondary: buttonColor,
        secondaryVariant: colorScheme.onSecondary,
        surface: colorScheme.onSurface,
        background: themeBackgroundColor,
        error: colorScheme.onError,
        onPrimary: colorScheme.primary,
        onSecondary: colorScheme.secondary,
        onSurface: colorScheme.surface,
        onBackground: colorScheme.background,
        onError: colorScheme.error,
        brightness: brightness,
      ),
      inputDecorationTheme: theme.inputDecorationTheme.copyWith(
        labelStyle: const TextStyle(
          decorationColor: Colors.black,
        ),
      ),
    );

    return Theme(
      data: inverseTheme,
      child: TextFormField(
        readOnly: true,
        controller: controller,
        obscureText: widget.obscureText,
        style: const TextStyle(
          color: Colors.black,
        ),
        keyboardType: TextInputType.datetime,
        autofillHints: widget.autofillHints,
        decoration: InputDecoration(
          hintText: widget.hintText,
          labelText: widget.label,
          labelStyle: const TextStyle(
            color: Colors.black,
          ),
          suffixIcon: Icon(widget.icon),
          border: const OutlineInputBorder(),
        ),
        onTap: () => _selectDate(context),
        onSaved: _onSave,
        validator: _validator,
      ),
    );
  }
}

class AppTimeFormField extends StatefulWidget {
  final IconData icon;
  final String hintText;
  final void Function(String?, DateTime?) onSave;
  final void Function(TextEditingController)? onController;
  final String? Function(String?, DateTime?)? onValidate;
  final bool obscureText;
  final TextInputType? keyboardType;
  final Iterable<String>? autofillHints;
  final String label;
  final Color labelColor;
  final TimeOfDay? initialTime;
  final DateTime? lastDate;

  const AppTimeFormField({
    Key? key,
    required this.icon,
    required this.hintText,
    required this.onSave,
    required this.label,
    this.onValidate,
    this.onController,
    this.obscureText = false,
    this.keyboardType,
    this.autofillHints,
    this.labelColor = Colors.black,
    this.lastDate,
    this.initialTime,
  }) : super(key: key);

  @override
  _AppTimeFormFieldState createState() => _AppTimeFormFieldState();
}

class _AppTimeFormFieldState extends State<AppTimeFormField> {
  late TextEditingController controller;

  final intl.DateFormat formatter = intl.DateFormat.jm('en_US');

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
    if (widget.initialTime != null) {
      controller.text = formatter.format(toDateTime(widget.initialTime!));
    }
    if (widget.onController != null) widget.onController!(controller);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  TimeOfDay timeOfDay = TimeOfDay.now();

  Future<void> _selectDate(BuildContext context) async {
    timeOfDay = await showTimePicker(
          context: context,
          initialTime: widget.initialTime ?? TimeOfDay.now(),
        ) ??
        TimeOfDay.now();
    final String formatted = formatter.format(toDateTime(timeOfDay));

    setState(() {
      controller.text = formatted;
    });
  }

  void _onSave(String? value) {
    widget.onSave(value, toDateTime(timeOfDay));
  }

  String? _validator(String? value) {
    if (widget.onValidate != null) {
      widget.onValidate!(value, toDateTime(timeOfDay));
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: widget.obscureText,
      keyboardType: TextInputType.datetime,
      autofillHints: widget.autofillHints,
      decoration: InputDecoration(
        hintText: widget.hintText,
        labelText: widget.label,
        suffixIcon: Icon(widget.icon),
        border: const OutlineInputBorder(),
      ),
      onTap: () => _selectDate(context),
      onSaved: _onSave,
      validator: _validator,
    );
  }
}

class AppGenderSelectionFormField extends StatefulWidget {
  final String hintText;
  final void Function(String?, FormGender?) onSave;
  final void Function(TextEditingController)? onController;
  final String? Function(String?, FormGender?)? onValidate;
  final bool obscureText;
  final TextInputType? keyboardType;
  final Iterable<String>? autofillHints;
  final String label;
  final Color labelColor;
  final FormGender? initialValue;

  const AppGenderSelectionFormField({
    Key? key,
    required this.hintText,
    required this.onSave,
    required this.label,
    this.onValidate,
    this.onController,
    this.obscureText = false,
    this.keyboardType,
    this.autofillHints,
    this.labelColor = Colors.black,
    this.initialValue,
  }) : super(key: key);

  @override
  _AppGenderSelectionFormFieldState createState() =>
      _AppGenderSelectionFormFieldState();
}

enum FormGender {
  // ignore: constant_identifier_names
  Male,
  // ignore: constant_identifier_names
  Female,
  // ignore: constant_identifier_names
  Prefer_Not_To_Say,
}

extension _$G on FormGender {
  String get text => describeEnum(this).replaceAll('_', ' ');
}

class _AppGenderSelectionDialog extends StatelessWidget {
  final FormGender selectedGender;

  const _AppGenderSelectionDialog({
    Key? key,
    this.selectedGender = FormGender.Prefer_Not_To_Say,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      width: 300,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: FormGender.values.length,
        padding: const EdgeInsets.all(8.0),
        itemBuilder: (BuildContext context, int index) {
          final _value = FormGender.values[index];
          return ListTile(
            selected: _value == selectedGender,
            onTap: () {
              Navigator.pop(context, _value);
            },
            title: Text(_value.text),
          );
        },
      ),
    );
  }
}

class _AppGenderSelectionFormFieldState
    extends State<AppGenderSelectionFormField> {
  late TextEditingController controller;

  final intl.DateFormat formatter = intl.DateFormat('dd/MM/yyyy');

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
    if (widget.initialValue != null) {
      controller.text = widget.initialValue!.text;
    }
    if (widget.onController != null) widget.onController!(controller);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  FormGender _selectedGender = FormGender.Prefer_Not_To_Say;

  Future<void> _selectDate(BuildContext context) async {
    _selectedGender = await showDialog<FormGender>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Select Gender'),
              content: _AppGenderSelectionDialog(
                selectedGender: _selectedGender,
              ),
            );
          },
        ) ??
        FormGender.Prefer_Not_To_Say;

    setState(() {
      controller.text = _selectedGender.text;
    });
  }

  void _onSave(String? value) {
    widget.onSave(value, _selectedGender);
  }

  String? _validator(String? value) {
    if (widget.onValidate != null) {
      widget.onValidate!(value, _selectedGender);
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: widget.obscureText,
      keyboardType: TextInputType.text,
      autofillHints: widget.autofillHints,
      decoration: InputDecoration(
        hintText: widget.hintText,
        labelText: widget.label,
        border: const OutlineInputBorder(),
      ),
      onTap: () => _selectDate(context),
      onSaved: _onSave,
      validator: _validator,
    );
  }
}

class DropDownSelectionField<T extends Enum> extends StatefulWidget {
  final Iterable<T> enumValues;
  final T? initialValue;
  final String labelText;
  final String? hintText;
  final Widget? suffixIcon;
  final FormFieldSetter<T> onSave;
  final FormFieldValidator<T>? onValidate;

  const DropDownSelectionField({
    Key? key,
    required this.enumValues,
    required this.labelText,
    this.initialValue,
    this.hintText,
    this.suffixIcon,
    required this.onSave,
    this.onValidate,
  }) : super(key: key);

  @override
  _DropDownSelectionFieldState<T> createState() =>
      _DropDownSelectionFieldState<T>();
}

class _DropDownSelectionFieldState<T extends Enum>
    extends State<DropDownSelectionField<T>> {
  T? _selectedItem;

  late final List<DropdownMenuItem<T>> _dropDownMenuItems;

  @override
  void initState() {
    _selectedItem = widget.initialValue;
    _dropDownMenuItems = widget.enumValues.map<DropdownMenuItem<T>>(
      (it) {
        return DropdownMenuItem<T>(
          value: it,
          // onTap: () {
          //   setState(() {
          //     _selectedItem = it;
          //   });
          // },
          child: Text(it.text),
        );
      },
    ).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<T>(
      items: _dropDownMenuItems,
      decoration: InputDecoration(
        labelText: widget.labelText,
        hintText: widget.hintText,
        suffixIcon: widget.suffixIcon,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        border: const OutlineInputBorder(),
      ),
      validator: widget.onValidate,
      onSaved: widget.onSave,
      onChanged: (value) {
        setState(() {
          _selectedItem = value;
        });
      },
      value: _selectedItem,
    );
  }
}
