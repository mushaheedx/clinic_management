import 'package:clinic_management/widget/form_field.dart';
import 'package:clinic_management/widget/suggestions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class InputChipLabels with ChangeNotifier {
  final _chipLabels = <String>[];

  bool get isChipsContainerEmpty => _chipLabels.isEmpty;
  bool get isChipsContainerNotEmpty => _chipLabels.isNotEmpty;

  List<String> get addableChipLabels => _chipLabels;

  @mustCallSuper
  void addChip(String value) {
    _chipLabels.add(value);
    notifyListeners();
  }

  @mustCallSuper
  void deleteChip(String value) {
    _chipLabels.remove(value);
    notifyListeners();
  }
}

typedef InputChipLabelsProviderCallback = InputChipLabels Function(
  ScopedReader watch,
);

typedef FetchStringSuggestionsCallback = Future<Iterable<String?>?> Function(
  String value,
);

class ChipsAddableAndSuggestibleField extends StatefulWidget {
  final InputChipLabelsProviderCallback getChipLabels;
  final FormFieldSetter<String> onSave;
  final FetchStringSuggestionsCallback fetchSuggestions;
  final FormFieldValidator<String> onValidate;
  final String labelText;
  final String hintText;

  const ChipsAddableAndSuggestibleField({
    Key? key,
    required this.onSave,
    required this.fetchSuggestions,
    required this.getChipLabels,
    required this.onValidate,
    required this.labelText,
    required this.hintText,
  }) : super(key: key);

  @override
  _ChipsAddableAndSuggestibleFieldState createState() =>
      _ChipsAddableAndSuggestibleFieldState();
}

class _ChipsAddableAndSuggestibleFieldState
    extends State<ChipsAddableAndSuggestibleField> {
  final TextFormFieldTools tools = TextFormFieldTools();
  late final ScrollController scrollController;

  @override
  void initState() {
    scrollController = ScrollController();
    tools.onInitState();
    super.initState();
  }

  @override
  void dispose() {
    tools.onDispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, _) {
      final _chipsContainer = widget.getChipLabels(watch);

      return Column(
        children: [
          Builder(
            builder: (context) {
              if (_chipsContainer.isChipsContainerEmpty) {
                return const SizedBox();
              }

              final _children =
                  _chipsContainer.addableChipLabels.map<Widget>((it) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 4.0,
                    vertical: 2.0,
                  ),
                  child: InputChip(
                    label: Text(it),
                    onDeleted: () {
                      _chipsContainer.deleteChip(it);
                    },
                  ),
                );
              }).toList();

              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Row(
                  children: [
                    Wrap(
                      children: _children,
                    ),
                  ],
                ),
              );
            },
          ),
          SuggestibleTextField<Iterable<String?>?>(
            fetchSuggestions: widget.fetchSuggestions,
            textEditingController: tools.controller,
            fieldFocusNode: tools.focusNode,
            onDataBuilder: (context, onTap, result) {
              if (result?.isEmpty ?? true) return const SizedBox();
              return Material(
                elevation: 4,
                child: CupertinoScrollbar(
                  isAlwaysShown: true,
                  controller: scrollController,
                  child: ListView.builder(
                    itemCount: result!.length,
                    controller: scrollController,
                    itemBuilder: (context, index) {
                      final _item = result.elementAt(index);
                      return ListTile(
                        onTap: () {
                          onTap(() {
                            if (_item?.isNotEmpty ?? false) {
                              _chipsContainer.addChip(_item!);
                              tools.replaceAllText('');
                            }
                          });
                        },
                        title: Text(_item ?? ''),
                      );
                    },
                  ),
                ),
              );
            },
            onLoadingBuilder: (context) {
              return Material(
                elevation: 4,
                child: Column(
                  children: const [
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: CircularProgressIndicator(),
                    ),
                  ],
                ),
              );
            },
            child: AppTextFormField(
              tools: tools,
              labelText: widget.labelText,
              hintText: widget.hintText,
              onValidate: widget.onValidate,
              onSave: (_) {
                final value = _chipsContainer.addableChipLabels.join(', ');
                widget.onSave(value);
              },
              onEditingComplete: () {
                final value = tools.controller.text;
                if (value.isNotEmpty) {
                  _chipsContainer.addChip(value.trim().replaceAll(',', ''));
                  tools.replaceAllText('');
                }
              },
              suffixIcon: InkWell(
                onTap: () {
                  tools.replaceAllText('');
                },
                child: const Tooltip(
                  message: 'Clear field',
                  child: Icon(
                    Icons.clear_rounded,
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    });
  }
}
