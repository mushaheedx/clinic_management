import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

typedef SuggestionsCallback<RESULTS> = Future<RESULTS> Function(
  String query,
);

typedef SuggestionsErrorBuilder = Widget Function(
  Object error,
  StackTrace? stackTrace,
);

typedef OnItemTapCallback = void Function(
  VoidCallback callback,
);

typedef ResultWidgetBuilder<RESULTS> = Widget Function(
  BuildContext context,
  OnItemTapCallback onItemTapCallback,
  RESULTS result,
);

typedef QueryStringBuilderCallback = String Function(
  TextEditingController controller,
);

class SuggestibleTextField<RESULTS> extends StatefulWidget {
  final FocusNode fieldFocusNode;
  final TextEditingController textEditingController;
  final BoxConstraints? constraints;
  final Widget child;
  final QueryStringBuilderCallback? buildQueryString;
  final SuggestionsCallback<RESULTS> fetchSuggestions;

  final ResultWidgetBuilder<RESULTS> onDataBuilder;
  final WidgetBuilder? onLoadingBuilder;
  final SuggestionsErrorBuilder? onErrorBuilder;

  const SuggestibleTextField({
    required this.fieldFocusNode,
    required this.textEditingController,
    required this.fetchSuggestions,
    required this.onDataBuilder,
    required this.child,
    this.buildQueryString,
    this.constraints,
    this.onLoadingBuilder,
    this.onErrorBuilder,
  });

  @override
  _SuggestibleTextFieldState<RESULTS> createState() =>
      _SuggestibleTextFieldState();
}

class SearchResultsNotifier<RESULTS>
    extends StateNotifier<AutoDisposeFutureProvider<RESULTS>?> {
  final SuggestionsCallback<RESULTS> fetchSuggestions;
  final ProviderReference ref;
  SearchResultsNotifier(this.ref, this.fetchSuggestions) : super(null);

  late final _searchSuggestionsProviderFamily =
      FutureProvider.autoDispose.family<RESULTS, String>(
    (ref, query) {
      return fetchSuggestions(query);
    },
  );

  void cancel() {
    if (state != null) {
      final _value = ref.read(state!.future);
      _value.timeout(Duration.zero);
    }
  }

  @override
  void dispose() {
    cancel();
    super.dispose();
  }

  void execute(String query) {
    state = _searchSuggestionsProviderFamily.call(query);
  }
}

class _SuggestibleTextFieldState<RESULTS>
    extends State<SuggestibleTextField<RESULTS>> {
  final _layerLink = LayerLink();
  late final StateNotifierProvider<SearchResultsNotifier<RESULTS>,
      AutoDisposeFutureProvider<RESULTS>?> searchStateNotifier;

  @override
  void initState() {
    Fimber.i('Initializing Suggestible Text Field');
    searchStateNotifier = StateNotifierProvider<SearchResultsNotifier<RESULTS>,
        AutoDisposeFutureProvider<RESULTS>?>((ref) {
      return SearchResultsNotifier<RESULTS>(ref, widget.fetchSuggestions);
    });
    widget.textEditingController.addListener(_onSearchQueryChange);
    widget.fieldFocusNode.addListener(_setupSuggestionsWithFieldFocusNode);
    super.initState();
  }

  @override
  void dispose() {
    widget.textEditingController.removeListener(_onSearchQueryChange);
    widget.fieldFocusNode.removeListener(_setupSuggestionsWithFieldFocusNode);
    _tearDown();
    super.dispose();
  }

  void _setupSuggestionsWithFieldFocusNode() {
    Fimber.i('Attached Field Focus Listener');
    if (widget.fieldFocusNode.hasFocus) {
      _insertOverlay();
    } else {
      _removeOverlay();
    }
  }

  OverlayEntry? _overlayEntry;

  void _insertOverlay() {
    Fimber.i('Building overlay entry');
    _overlayEntry = _buildOverlayEntry();
    Fimber.i('Inserting overlay entry to overlay');
    Overlay.of(context)?.insert(_overlayEntry!);
  }

  void _removeOverlay() {
    Fimber.i('Removing overlay entry from overlay upon losing focus');
    _overlayEntry?.remove();
    _overlayEntry = null;

    final _searchState = context.read(searchStateNotifier.notifier);
    _searchState.cancel();
  }

  bool _wasAnItemTappedByUser = false;

  void _tearDown() {
    _overlayEntry?.remove();
    _overlayEntry = null;
    _wasAnItemTappedByUser = false;
  }

  void _onItemTapCallback(VoidCallback callback) {
    _wasAnItemTappedByUser = true;
    callback();
  }

  void _onSearchQueryChange() {
    Fimber.i('Noticed search query changes');
    Fimber.i(
        'Position: ${widget.textEditingController.selection.extentOffset}, length: ${widget.textEditingController.text.length}');
    final _searchState = context.read(searchStateNotifier.notifier);

    // Cancel previous query
    _searchState.cancel();

    if (_wasAnItemTappedByUser) {
      _wasAnItemTappedByUser = false;
      return;
    }

    // Create new query
    final String query;

    if (widget.buildQueryString != null) {
      query = widget.buildQueryString!(widget.textEditingController);
    } else {
      query = widget.textEditingController.text.trim();
    }

    if (query.isEmpty) {
      return;
    }

    if (_overlayEntry == null) _insertOverlay();

    _searchState.execute(query);
  }

  OverlayEntry _buildOverlayEntry() {
    final _size = context.size ?? Size.zero;
    return OverlayEntry(
      builder: (context) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            _removeOverlay();
          },
          child: Stack(
            children: [
              Positioned(
                width: _size.width,
                child: CompositedTransformFollower(
                  link: _layerLink,
                  showWhenUnlinked: false,
                  offset: Offset(0.0, _size.height + 5.0),
                  // Reactable widget
                  child: ConstrainedBox(
                    constraints: widget.constraints ??
                        const BoxConstraints(
                          maxHeight: 200,
                        ),
                    child: ReactiveSuggestionsBuilder<RESULTS>(
                      searchStateNotifier: searchStateNotifier,
                      onDataBuilder: widget.onDataBuilder,
                      onloadingBuilder: widget.onLoadingBuilder,
                      onErrorBuilder: widget.onErrorBuilder,
                      onItemTap: _onItemTapCallback,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
      link: _layerLink,
      child: widget.child,
    );
  }
}

class ReactiveSuggestionsBuilder<RESULTS> extends ConsumerWidget {
  final StateNotifierProvider<SearchResultsNotifier<RESULTS>,
      AutoDisposeFutureProvider<RESULTS>?> searchStateNotifier;

  final ResultWidgetBuilder<RESULTS> onDataBuilder;
  final WidgetBuilder? onloadingBuilder;
  final SuggestionsErrorBuilder? onErrorBuilder;
  final OnItemTapCallback onItemTap;

  const ReactiveSuggestionsBuilder({
    Key? key,
    required this.searchStateNotifier,
    required this.onDataBuilder,
    required this.onloadingBuilder,
    required this.onErrorBuilder,
    required this.onItemTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader reader) {
    final searchState = reader(searchStateNotifier);
    if (searchState == null) return const SizedBox();

    final _searchResults = reader(searchState);

    return _searchResults.when(
      data: (result) => onDataBuilder(context, onItemTap, result),
      loading: () {
        if (onloadingBuilder == null) return const SizedBox();
        return onloadingBuilder!(context);
      },
      error: onErrorBuilder ?? (_, __) => const SizedBox(),
    );
  }
}
