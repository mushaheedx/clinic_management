import 'package:flutter/widgets.dart';

class OnError extends StatelessWidget {
  final dynamic? e;
  final StackTrace? t;

  const OnError(this.e, this.t);

  @override
  Widget build(BuildContext context) {
    return const Text('Something went wrong');
  }
}
