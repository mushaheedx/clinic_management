// import 'package:clinic_management/common/asset.dart';
import 'package:clinic_management/common/insets.dart';
import 'package:clinic_management/common/styles.dart';

import 'package:flutter/material.dart';

class AppLogo extends StatelessWidget {
  final BoxFit fit;

  final double? height;
  final Color? color;

  const AppLogo({
    this.color,
    this.fit = BoxFit.contain,
    this.height = 42,
  });

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'application/icon',
      child: Padding(
        padding: AppInsets.all8,
        child: Material(
          color: Colors.transparent,
          child: Padding(
            padding: AppInsets.all8,
            child: SizedBox(
              height: height ?? 42,
              child: FittedBox(
                fit: BoxFit.fitHeight,
                child: Text(
                  'CLINIC CMS',
                  style: AppTheme.textTheme.headline4!.copyWith(
                    color: color ?? Colors.white,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
