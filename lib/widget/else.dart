import 'package:flutter/material.dart';

class ElseWrap extends StatelessWidget {
  final Widget child;
  final double verticalPadding;

  const ElseWrap({
    Key? key,
    this.verticalPadding = 16.0,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: verticalPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          child,
        ],
      ),
    );
  }
}
