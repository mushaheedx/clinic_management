import 'package:flutter/material.dart';

const _searchBorderRadius = BorderRadius.all(Radius.circular(35.0));

const _searchIcon = Icon(
  Icons.search_outlined,
);

class SearchAction extends StatelessWidget {
  final VoidCallback onPressed;

  const SearchAction({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _mediaQuery = MediaQuery.of(context);
    final _shortestSide = _mediaQuery.size.shortestSide;
    if (_shortestSide < 700) {
      return IconButton(
        icon: _searchIcon,
        color: Colors.white,
        onPressed: onPressed,
      );
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        color: Colors.grey.shade100,
        borderRadius: _searchBorderRadius,
        child: InkWell(
          onTap: onPressed,
          borderRadius: _searchBorderRadius,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
              ),
              child: Row(
                children: [
                  Text(
                    'Search',
                    style: Theme.of(context).textTheme.subtitle1?.copyWith(
                          color: Colors.black,
                        ),
                  ),
                  const SizedBox(width: 60.0),
                  IconTheme.merge(
                    data: const IconThemeData(
                      color: Colors.black,
                    ),
                    child: _searchIcon,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
