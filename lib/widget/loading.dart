import 'package:clinic_management/common/styles.dart';
import 'package:flutter/material.dart';

class LoadingDialog {
  BuildContext? _dialogContext;

  void hide() {
    if (_dialogContext == null) return;
    if (!Navigator.of(_dialogContext!).mounted) return;
    Navigator.of(_dialogContext!).pop();
  }

  Future<void> show({
    required BuildContext context,
    required String message,
  }) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        _dialogContext = context;
        return SimpleDialog(
          backgroundColor: Colors.white,
          children: [
            const Center(
              child: Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: CircularProgressIndicator(
                  valueColor: AppTheme.progressValueColor,
                ),
              ),
            ),
            if (message.isNotEmpty)
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    message,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
          ],
        );
      },
    );
  }
}
