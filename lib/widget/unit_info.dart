import 'package:flutter/material.dart';

class UnitInfo extends StatelessWidget {
  final String title;
  final String value;

  const UnitInfo({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    final _textTheme = _theme.textTheme;
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 12.0,
      ),
      child: Wrap(
        children: [
          Text(
            '$title:',
            style: _textTheme.subtitle1?.copyWith(
              color: Colors.white,
            ),
          ),
          const SizedBox(
            width: 8.0,
          ),
          Text(
            value,
            style: _textTheme.bodyText1?.copyWith(
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
