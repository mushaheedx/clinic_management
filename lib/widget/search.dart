import 'package:clinic_management/delegate/patient_record_search.dart';
import 'package:clinic_management/di/patients.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

const _searchBorderRadius = BorderRadius.all(Radius.circular(35.0));

class AppSearchBar extends StatelessWidget {
  const AppSearchBar();

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final CardTheme cardTheme = CardTheme.of(context);
    return Column(
      children: [
        ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Container(
            margin: cardTheme.margin ?? const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(bottom: 16),
            child: Material(
              type: MaterialType.card,
              shadowColor: cardTheme.shadowColor ?? theme.shadowColor,
              color: cardTheme.color ?? theme.cardColor,
              elevation: cardTheme.elevation ?? 2.0,
              shape: const RoundedRectangleBorder(
                borderRadius: _searchBorderRadius,
              ),
              clipBehavior: cardTheme.clipBehavior ?? Clip.none,
              child: InkWell(
                onTap: () async {
                  final _data = await context.read(patientsDataProvider.future);
                  final _records = _data.data ?? const [];
                  await showSearch(
                    context: context,
                    delegate: PatientRecordsSearchDelegate(_records),
                  );
                },
                borderRadius: _searchBorderRadius,
                child: const ListTile(
                  leading: Icon(Icons.search),
                  title: Text('Search'),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
