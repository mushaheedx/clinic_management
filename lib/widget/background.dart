import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';

class AppBackground extends StatelessWidget {
  final Widget child;

  const AppBackground({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          tileMode: TileMode.mirror,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color(0xfffb3b2d),
            Color(0xff2a9af4),
            Color(0xff2c9e8c),
          ],
          stops: [
            0,
            0.5,
            1,
          ],
        ),
        backgroundBlendMode: BlendMode.srcOver,
      ),
      child: PlasmaRenderer(
        color: const Color(0x44e45a23),
        blur: 0.4,
        size: 0.83,
        speed: 1.58,
        offset: 0.33,
        blendMode: BlendMode.plus,
        particleType: ParticleType.atlas,
        child: child,
      ),
    );
  }
}
