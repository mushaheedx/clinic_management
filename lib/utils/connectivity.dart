import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';

class ConnectionHelper {
  ConnectionHelper._();

  static Future<bool> get hasConnection async {
    if (kIsWeb) return true;
    if (Platform.isLinux || Platform.isWindows) return true;
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.
      // API
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.
      // API
      return true;
    } else {
      return false;
    }
  }
}
