// ignore: avoid_web_libraries_in_flutter
import 'remove_loader_web.dart' if (dart.library.io) 'remove_loader_stub.dart';

/// Removes `loading` div from html
///
/// Refer https://stackoverflow.com/a/63933407
void removeLoaderFromHtml() {
  $removeLoaderFromHtml();
}
