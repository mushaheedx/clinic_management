// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

void $removeLoaderFromHtml() {
  final loader = document.getElementsByClassName('loading_indicator');
  if (loader.isNotEmpty) {
    loader.first.remove();
  }
}
