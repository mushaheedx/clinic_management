class ScorableItem with Comparable<ScorableItem> {
  final String value;
  final Object? object;

  int _score = 0;

  int get score => _score;

  ScorableItem(this.value, [this.object]);

  void _addScore(int addition) {
    _score += addition;
  }

  void evaluate(String query) {
    _score = 0;
    if (query == value) {
      _addScore(100);
    } else if (value.toLowerCase() == query.toLowerCase()) {
      _addScore(75);
    } else if (value.startsWith(query)) {
      _addScore(50);
    } else if (value.contains(query)) {
      _addScore(25);
    } else {
      final _valueTrim = value.trim();
      final _queryTrim = query.trim();
      if (_valueTrim == _queryTrim) {
        _addScore(95);
      } else if (_valueTrim.toLowerCase() == _queryTrim.toLowerCase()) {
        _addScore(70);
      } else if (_valueTrim.startsWith(_queryTrim)) {
        _addScore(45);
      } else if (_valueTrim.contains(_queryTrim)) {
        _addScore(20);
      } else {
        final _valueTrimLower = _valueTrim.toLowerCase();
        final _queryTrimLower = _queryTrim.toLowerCase();
        if (_valueTrimLower == _queryTrimLower) {
          _addScore(90);
        } else if (_valueTrimLower.startsWith(_queryTrimLower)) {
          _addScore(40);
        } else if (_valueTrimLower.contains(_queryTrimLower)) {
          _addScore(15);
        } else {
          final _onlyWordAndNumbers = RegExp(r"[^\w]");
          final _valueStrip =
              _valueTrimLower.replaceAll(_onlyWordAndNumbers, '');
          final _queryStrip =
              _queryTrimLower.replaceAll(_onlyWordAndNumbers, '');
          if (_valueStrip == _queryStrip) {
            _addScore(85);
          } else if (_valueStrip.startsWith(_queryStrip)) {
            _addScore(35);
          } else if (_valueStrip.contains(_queryStrip)) {
            _addScore(10);
          }
        }
      }
    }
  }

  bool _removeIfNotRelevant(String query) {
    return score == 0;
  }

  @override
  int compareTo(ScorableItem other) {
    return other.score - score;
  }
}

class ScoringSearch {
  List<String> inStrings(
    final List<String> data,
    final String query,
  ) {
    final result = data.map((e) {
      final si = ScorableItem(e);
      si.evaluate(query);
      return si;
    }).toList();

    result.removeWhere((element) => element._removeIfNotRelevant(query));

    result.sort();

    return result.map((e) => e.value).toList();
  }

  List<String> inScorableObjects(
    final List<ScorableItem> data,
    final String query,
  ) {
    final _result = data.toList();
    for (final item in _result) {
      item.evaluate(query);
    }

    _result.removeWhere((element) => element._removeIfNotRelevant(query));

    _result.sort();

    return _result.map((e) => e.value).toList();
  }
}
