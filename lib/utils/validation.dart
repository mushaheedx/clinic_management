import 'package:flutter/widgets.dart';

String? cannotBeEmpty(String fieldName, String? value) {
  if (value?.trim().isEmpty ?? true) return '$fieldName cannot be empty';
  return null;
}

String wordAtCursorOffset(TextEditingController controller) {
  if (controller.text.isEmpty) return '';

  if (!controller.text.contains(',')) return controller.text;

  final _composingRange = computeRelevantComposingRange(controller);

  if (!_composingRange.isNormalized) {
    return '';
  }

  final _composingRangeText = _composingRange.textInside(controller.text);

  _composingRangeText.replaceAll(',', '');

  return _composingRangeText.trim();
}

/// ','
TextRange computeRelevantComposingRange(TextEditingController controller) {
  final _targetText = controller.text;

  if (_targetText.isEmpty) return TextRange.empty;

  // ignore: prefer_const_declarations
  final int _textBegin = 0;
  final int _textEnd = _targetText.length - 1;

  if (_targetText == ',') {
    return TextRange.empty;
  }

  // Bring cursor within text length range
  final _cursorPosition = controller.selection.extentOffset.clamp(
    _textBegin,
    _textEnd,
  );

  int _start = 0;
  int _end = 0;

  // Calculate base offset
  for (var i = _cursorPosition - 1; i >= _textBegin; i--) {
    if (i < _textBegin) {
      // not found
      _start = _textBegin;
      break;
    }

    final _character = _targetText[i];
    if (_character == ',') {
      if (i == _textEnd) {
        return TextRange.empty;
      }
      _start = i + 1;
      break;
    }
  }

  // Calculate extent offset
  for (var i = _cursorPosition; i <= _textEnd; i++) {
    if (i > _textEnd) {
      // not found
      _end = _textEnd;
      break;
    }

    final _character = _targetText[i];
    if (_character == ',') {
      if (i == _textBegin) {
        return TextRange.empty;
      }
      _end = i - 1;
      break;
    }
  }

  final _startF = _start.clamp(-1, _textEnd);
  final _endF = _end.clamp(-1, _textEnd);

  return TextRange(
    start: _startF,
    end: _endF,
  );
}
