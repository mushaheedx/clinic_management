typedef CopyWithCallback<T> = T Function(T data);

class CopyableDetails<T> {
  T _data;

  T get data => _data;

  CopyableDetails(this._data);

  void copyWith(CopyWithCallback<T> copyWithAction) {
    _data = copyWithAction(data);
  }
}
