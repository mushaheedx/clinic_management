import 'package:equatable/equatable.dart';

class Enum<T extends Object> with EquatableMixin {
  final T value;

  Enum(this.value);

  static Iterable<Enum<T>> from<T extends Object>(List<T> values) {
    return values.map<Enum<T>>((it) => Enum<T>(it));
  }

  String get text {
    final String description = value.toString();
    final int indexOfDot = description.indexOf('.');
    assert(
      indexOfDot != -1 && indexOfDot < description.length - 1,
      'The provided object "$value" is not an enum.',
    );
    final _res = description.substring(indexOfDot + 1);
    return _res.replaceAll('_', ' ');
  }

  @override
  List<Object> get props => [
        value,
      ];
}
