import 'package:clinic_management/di/navigation.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'common/strings.dart';
import 'common/styles.dart';

void main() {
  final _debugTree = DebugTree.elapsed();

  Fimber.plantTree(_debugTree);

  runApp(const ProviderScope(
    child: MyApp(),
  ));
}

class MyApp extends ConsumerWidget {
  const MyApp();

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final _navigation = watch(navigationProvider);

    return MaterialApp(
      navigatorKey: _navigation.navigatorKey,
      title: AppText.applicationTitle,
      onGenerateRoute: _navigation.generateRoute,
      theme: ThemeData(
        primarySwatch: AppTheme.primarySwatch,
        brightness: Brightness.dark,
        textTheme: AppTheme.textTheme,
        primaryTextTheme: AppTheme.textTheme,
        accentTextTheme: AppTheme.textTheme,
        appBarTheme: const AppBarTheme(
          centerTitle: true,
        ),
      ),
    );
  }
}
