import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/di/patients.dart';
import 'package:clinic_management/utils/enum.dart';
import 'package:clinic_management/utils/validation.dart';
import 'package:clinic_management/widget/form_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

const _height = SizedBox(
  height: 16,
);

class AddPatientScreen extends StatefulWidget {
  const AddPatientScreen();

  @override
  _AddPatientScreenState createState() => _AddPatientScreenState();
}

class _AddPatientScreenState extends State<AddPatientScreen> {
  final addablePatientDetails = getAddableDetailsOfPatient();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Patient'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 12,
        ),
        children: [
          Column(
            children: [
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 600),
                child: Form(
                  key: formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 25,
                      ),
                      AppTextFormField(
                        labelText: 'OPD No.',
                        onValidate: (value) => cannotBeEmpty('OPD No.', value),
                        onSave: (value) {
                          addablePatientDetails
                              .copy((data) => data.copyWith(opdId: value));
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Name',
                        keyboardType: TextInputType.name,
                        onValidate: (value) => cannotBeEmpty('Name', value),
                        onSave: (value) {
                          addablePatientDetails
                              .copy((data) => data.copyWith(name: value));
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Age',
                        keyboardType: TextInputType.number,
                        onValidate: (value) => cannotBeEmpty('Age', value),
                        onSave: (value) {
                          addablePatientDetails
                              .copy((data) => data.copyWith(age: value));
                        },
                      ),
                      _height,
                      DropDownSelectionField<Enum<FormGender>>(
                        enumValues: Enum.from(FormGender.values),
                        initialValue: Enum(FormGender.Prefer_Not_To_Say),
                        labelText: 'Gender',
                        hintText: "Patient's Gender",
                        onValidate: (value) {
                          if (value == null) {
                            return 'Gender cannot be empty';
                          }
                        },
                        onSave: (value) {
                          addablePatientDetails.copy(
                            (data) => data.copyWith(gender: value?.text),
                          );
                        },
                      ),
                      _height,
                      AppTextFormField(
                        maxLines: 3,
                        labelText: 'Address',
                        keyboardType: TextInputType.streetAddress,
                        onValidate: (value) => cannotBeEmpty('Address', value),
                        onSave: (value) {
                          addablePatientDetails
                              .copy((data) => data.copyWith(address: value));
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Contact Number',
                        keyboardType: TextInputType.phone,
                        onValidate: (value) =>
                            cannotBeEmpty('Contact Number', value),
                        onSave: (value) {
                          addablePatientDetails
                              .copy((data) => data.copyWith(mobile: value));
                        },
                      ),
                      _height,
                      AppDateFormField(
                        label: 'Date of first visit',
                        labelColor: Colors.white,
                        icon: Icons.calendar_today_outlined,
                        hintText: 'dd-mm-yyyy',
                        initialDate: DateTime.now(),
                        onValidate: (value, dateTime) =>
                            cannotBeEmpty('Date of first visit', value),
                        onSave: (value, dateTime) {
                          addablePatientDetails.copy(
                            (data) => data.copyWith(dateVisit: value),
                          );
                        },
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          final _formState = formKey.currentState;
                          if (_formState == null) {
                            Fimber.i('Form state null');
                            return;
                          }
                          if (_formState.validate()) {
                            _formState.save();
                            Fimber.i(
                                '${addablePatientDetails.data.data.toJson()}');
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: AppTheme.primarySwatch,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 16.0,
                            horizontal: 16,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Text('ADD PATIENT'),
                              Icon(
                                Icons.chevron_right_rounded,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
