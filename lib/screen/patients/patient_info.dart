import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/di/patients.dart';
import 'package:clinic_management/plain/patient_record.dart';
import 'package:clinic_management/screen/visits/visits.dart';
import 'package:clinic_management/widget/else.dart';
import 'package:clinic_management/widget/on_error.dart';
import 'package:clinic_management/widget/unit_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class _UnitInfoCard extends StatelessWidget {
  final PatientRecord record;

  const _UnitInfoCard({
    Key? key,
    required this.record,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.grey.shade800,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 8.0,
          vertical: 16.0,
        ),
        child: ConstrainedBox(
          constraints: const BoxConstraints(
            maxWidth: 700,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              UnitInfo(
                title: 'NAME',
                value: record.name ?? '',
              ),
              Row(
                children: [
                  UnitInfo(
                    title: 'OPD',
                    value: record.opdId ?? '',
                  ),
                  UnitInfo(
                    title: 'MOBILE',
                    value: record.mobile ?? '',
                  ),
                ],
              ),
              Row(
                children: [
                  UnitInfo(
                    title: 'AGE',
                    value: record.age ?? '',
                  ),
                  UnitInfo(
                    title: 'GENDER',
                    value: record.gender ?? '',
                  ),
                ],
              ),
              UnitInfo(
                title: 'ADDRESS',
                value: record.address ?? '',
              ),
              Wrap(
                children: [
                  UnitInfo(
                    title: 'FIRST VISIT',
                    value: record.dateVisit ?? '',
                  ),
                  UnitInfo(
                    title: 'LAST VISIT',
                    value: record.date ?? '',
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PersistentHeader extends SliverPersistentHeaderDelegate {
  final Widget widget;
  final double? extent;

  PersistentHeader({
    required this.widget,
    this.extent,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return widget;
  }

  @override
  double get maxExtent => extent ?? 56.0;

  @override
  double get minExtent => extent ?? 56.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}

class PatientInfoScreen extends ConsumerWidget {
  final PatientRecord record;

  const PatientInfoScreen({
    Key? key,
    required this.record,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader reader) {
    final patientRecordAsync = reader(patientInfoProvider(record));

    final _theme = Theme.of(context);

    final _textTheme = _theme.textTheme;

    return Scaffold(
      backgroundColor: Colors.grey.shade800,
      appBar: AppBar(
        title: SelectableText(
          patientRecordAsync.maybeWhen(
            data: (data) => data.data?.name ?? '',
            orElse: () => record.name ?? '',
          ),
        ),
      ),
      body: patientRecordAsync.when(
        data: (data) {
          if (data.data == null) {
            return OnError(data.error, data.stackTrace as StackTrace?);
          }
          final record = data.data!;

          return NestedScrollView(
            headerSliverBuilder: (
              BuildContext context,
              bool innerBoxIsScrolled,
            ) {
              return <Widget>[
                SliverPersistentHeader(
                  delegate: PersistentHeader(
                    extent: 270,
                    widget: _UnitInfoCard(
                      record: record,
                    ),
                  ),
                ),
                SliverPersistentHeader(
                  pinned: true,
                  delegate: PersistentHeader(
                    extent: 70,
                    widget: Material(
                      color: Colors.grey.shade900,
                      elevation: 4,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 22.0,
                          horizontal: 12.0,
                        ),
                        child: Row(
                          children: [
                            Text(
                              'VISIT HISTORY',
                              style: _textTheme.headline6,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ];
            },
            body: PatientVisitHistory(
              record: record,
            ),
          );
        },
        loading: () => ElseWrap(
          child: SizedBox.fromSize(
            size: const Size.square(25),
            child: const CircularProgressIndicator(),
          ),
        ),
        error: (e, s) => ElseWrap(
          child: OnError(e, s),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).pushNamed(AppRouteName.patientAddVisit);
        },
        icon: const Icon(Icons.assignment_turned_in_outlined),
        label: const Text('ADD VISIT'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
