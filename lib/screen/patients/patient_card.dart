import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/di/patients.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart' as intl;
import 'package:timeago/timeago.dart' as timeago;

import '../../widget/on_error.dart';

class PatientsCard extends ConsumerWidget {
  const PatientsCard();

  @override
  Widget build(BuildContext context, ScopedReader reader) {
    final _data = reader(patientsDataProvider);
    final _theme = Theme.of(context);
    final _textTheme = _theme.textTheme;
    final _dateFormatter = intl.DateFormat('yyyy-MM-dd hh:mm:ss');

    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Material(
        color: Colors.black38,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 22,
            horizontal: 22,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                flex: 0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    'Patients',
                    textAlign: TextAlign.start,
                    style: _textTheme.headline5,
                  ),
                ),
              ),
              Flexible(
                flex: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _data.when(
                      data: (data) {
                        if (data.data?.isEmpty ?? true) {
                          return OnError(
                            data.error,
                            data.stackTrace as StackTrace?,
                          );
                        }

                        final _data = data.data ?? [];

                        _data.sort();

                        final _numberOfRecordsToShow = _data.length.clamp(0, 5);

                        return Flexible(
                          child: DefaultTextStyle(
                            style: _textTheme.bodyText2!.copyWith(
                              fontWeight: FontWeight.w300,
                            ),
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: _numberOfRecordsToShow + 1,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                if (index == 0) {
                                  return DefaultTextStyle(
                                    style: _textTheme.subtitle2!.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 4.0),
                                      child: Row(
                                        children: const [
                                          Expanded(child: Text('Name')),
                                          Flexible(
                                            flex: 0,
                                            child: Text('Last Visit'),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                                final _patient = _data.elementAt(index + 1);
                                final DateTime? _date = _patient.date == null
                                    ? null
                                    : _dateFormatter.parse(_patient.date!);

                                final _formattedDate =
                                    _date != null ? timeago.format(_date) : '-';

                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 2.0,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: Text(_patient.name ?? '')),
                                      Flexible(
                                        flex: 0,
                                        child: Text(_formattedDate),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                      },
                      loading: () {
                        return SizedBox.fromSize(
                          size: const Size.square(25),
                          child: const CircularProgressIndicator(),
                        );
                      },
                      error: (e, t) => OnError(e, t),
                    ),
                  ],
                ),
              ),
              ButtonBar(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed(AppRouteName.patientRecords);
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                    ).copyWith(
                      overlayColor: MaterialStateProperty.resolveWith(
                        (states) {
                          return states.contains(MaterialState.pressed)
                              ? Colors.grey.shade200
                              : null;
                        },
                      ),
                    ),
                    child: const Text(
                      'SHOW ALL',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
