import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/delegate/patient_record_search.dart';
import 'package:clinic_management/di/patients.dart';
import 'package:clinic_management/widget/on_error.dart';
import 'package:clinic_management/widget/search_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart' as intl;
import 'package:timeago/timeago.dart' as timeago;

class PatientRecordsScreen extends ConsumerWidget {
  const PatientRecordsScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader reader) {
    final _data = reader(patientsDataProvider);
    final _theme = Theme.of(context);
    final _textTheme = _theme.textTheme;
    final _dateFormatter = intl.DateFormat('yyyy-MM-dd hh:mm:ss');

    return Scaffold(
      appBar: AppBar(
        title: const Text('Patient Records'),
        actions: [
          SearchAction(
            onPressed: () async {
              final _data = await context.read(patientsDataProvider.future);
              final _records = _data.data ?? const [];
              await showSearch(
                context: context,
                delegate: PatientRecordsSearchDelegate(_records),
              );
            },
          ),
          const SizedBox(
            width: 8,
          ),
          IconButton(
            onPressed: () {
              context.read(patientGridViewStatusProvider.notifier).toggle();
            },
            icon: Consumer(
              builder: (context, reader, _) {
                return Icon(
                  reader(patientGridViewStatusProvider)
                      ? Icons.grid_on_outlined
                      : Icons.grid_off_outlined,
                );
              },
            ),
          ),
          const SizedBox(
            width: 20,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 8.0,
          vertical: 16.0,
        ),
        child: Column(
          children: [
            Expanded(
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 8,
                    horizontal: 10,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        flex: 0,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Text(
                            'Patients',
                            textAlign: TextAlign.start,
                            style: _textTheme.headline6,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 0,
                        child: DefaultTextStyle(
                          style: _textTheme.subtitle2!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 2.0),
                            child: Row(
                              children: const [
                                Expanded(child: Text('Name')),
                                Flexible(
                                  flex: 0,
                                  child: Text('Last Visit'),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const Flexible(
                        flex: 0,
                        child: Divider(
                          thickness: 3,
                        ),
                      ),
                      _data.when(
                        data: (data) {
                          if (data.data?.isEmpty ?? true) {
                            return OnError(
                              data.error,
                              data.stackTrace as StackTrace?,
                            );
                          }

                          data.data?.sort();

                          return Flexible(
                            child: DefaultTextStyle(
                              style: _textTheme.bodyText2!.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                              child: ListView.separated(
                                shrinkWrap: true,
                                itemCount: data.data?.length ?? 0,
                                separatorBuilder: (context, index) {
                                  return Container(
                                    height: 1.0,
                                    width: double.infinity,
                                    color: DividerTheme.of(context).color ??
                                        Theme.of(context).dividerColor,
                                  );
                                },
                                itemBuilder: (context, index) {
                                  final _patient = data.data?.elementAt(index);
                                  if (_patient == null) {
                                    return AppDecoration.emptyBox;
                                  }
                                  final DateTime? _date = _patient.date == null
                                      ? null
                                      : _dateFormatter.parse(_patient.date!);

                                  final _formattedDate = _date != null
                                      ? timeago.format(_date)
                                      : '-';

                                  return TextButton(
                                    onPressed: () async {
                                      Navigator.of(context).pushNamed(
                                        AppRouteName.patientInfo,
                                        arguments: _patient,
                                      );
                                    },
                                    style: TextButton.styleFrom(
                                      primary: Colors.white,
                                      padding: EdgeInsets.zero,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12.0),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: Text(_patient.name ?? '')),
                                          Flexible(
                                            flex: 0,
                                            child: Text(_formattedDate),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          );
                        },
                        loading: () {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox.fromSize(
                                size: const Size.square(25),
                                child: const CircularProgressIndicator(),
                              ),
                            ],
                          );
                        },
                        error: (e, t) => Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            OnError(e, t),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).pushNamed(AppRouteName.patientRecordsAdd);
        },
        icon: const Icon(Icons.person_add_alt_1_outlined),
        label: const Text('ADD PATIENT'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
