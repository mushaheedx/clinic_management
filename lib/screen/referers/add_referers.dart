import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/di/referers.dart';
import 'package:clinic_management/plain/medical_referer.dart';
import 'package:clinic_management/utils/enum.dart';
import 'package:clinic_management/utils/validation.dart';
import 'package:clinic_management/widget/form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

const _height = SizedBox(
  height: 16,
);

class AddReferenceScreen extends StatefulWidget {
  const AddReferenceScreen({
    Key? key,
  }) : super(key: key);
  @override
  _AddReferenceScreenState createState() => _AddReferenceScreenState();
}

class _AddReferenceScreenState extends State<AddReferenceScreen> {
  final addableMedicalReferenceForm = getAddableMedicalReferenceForm();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextFormFieldTools medicineFieldTools = TextFormFieldTools();

  @override
  void initState() {
    medicineFieldTools.onInitState();
    super.initState();
  }

  @override
  void dispose() {
    medicineFieldTools.onDispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add a Medicine'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 12,
        ),
        children: [
          Column(
            children: [
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 600),
                child: Form(
                  key: formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 25,
                      ),
                      AppTextFormField(
                        labelText: 'Reference name',
                        hintText: 'Reference',
                        onValidate: (value) =>
                            cannotBeEmpty('Reference name', value),
                        onSave: (value) {
                          addableMedicalReferenceForm
                              .copy((data) => data.copyWith(reference: value));
                        },
                      ),
                      _height,
                      DropDownSelectionField<Enum<ReferenceType>>(
                        enumValues: Enum.from(ReferenceType.values),
                        labelText: 'Reference Type',
                        hintText: 'Reference Type',
                        onSave: (value) {
                          addableMedicalReferenceForm.copy(
                            (data) => data.copyWith(
                              referenceType: value?.text,
                            ),
                          );
                        },
                        onValidate: (value) {
                          return cannotBeEmpty('Reference name', value?.text);
                        },
                      ),
                      _height,
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          final _formState = formKey.currentState;
                          if (_formState == null) {
                            Fimber.i('Form state null');
                            return;
                          }
                          if (_formState.validate()) {
                            _formState.save();
                            Fimber.i(
                                '${addableMedicalReferenceForm.data.data.toJson()}');
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: AppTheme.primarySwatch,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 16.0,
                            horizontal: 16,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Text('SUBMIT DETAILS'),
                              Icon(
                                Icons.chevron_right_rounded,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
