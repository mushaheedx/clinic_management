import 'package:clinic_management/common/insets.dart';
import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/common/strings.dart';
import 'package:clinic_management/di/authentication.dart';
import 'package:clinic_management/model/authentication.dart';
import 'package:clinic_management/plain/login.dart';
import 'package:clinic_management/widget/background.dart';
import 'package:clinic_management/widget/form_field.dart';
import 'package:clinic_management/widget/loading.dart';
import 'package:clinic_management/widget/logo.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class LoginScreen extends ConsumerWidget {
  final formKey = GlobalKey<FormState>();

  final form = LoginForm();

  Future<void> _submit(
      BuildContext context, AppAuthentication appAuthentication) async {
    final _formState = formKey.currentState;
    if (_formState?.validate() ?? false) {
      _formState!.save();
      Fimber.i('Form saved');
      final _loader = LoadingDialog();
      _loader.show(
        context: context,
        message: 'Logging in',
      );

      final _message = await appAuthentication.login(form);
      _loader.hide();
      if (appAuthentication.isLoggedIn) {
        Navigator.of(context).pushNamedAndRemoveUntil(
          AppRouteName.dashboard,
          (route) => false,
        );
        return;
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            _message ?? AppText.loginFailed,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final _theme = Theme.of(context);
    final CardTheme _cardTheme = CardTheme.of(context);
    final _mediaQuery = MediaQuery.of(context);
    final _topInsets = _mediaQuery.size.height * 0.2;

    final _appAuthenticationAsyncValue = watch(appAuthenticationProvider);

    return AppBackground(
      child: Scaffold(
        backgroundColor: Colors.black38,
        body: ListView(
          children: [
            SizedBox(
              height: _topInsets,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: Container(
                    padding: AppInsets.all16,
                    constraints: const BoxConstraints(
                      maxWidth: 400,
                    ),
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 20.0,
                            vertical: 8.0,
                          ),
                          child: AppLogo(
                            height: null,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        Container(
                          margin:
                              _cardTheme.margin ?? const EdgeInsets.all(4.0),
                          child: Material(
                            type: MaterialType.card,
                            shadowColor: Colors.transparent,
                            color: Colors.transparent,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 20.0),
                              child: Form(
                                key: formKey,
                                child: Column(
                                  children: [
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      AppText.login,
                                      style:
                                          _theme.textTheme.headline2?.copyWith(
                                        color: Colors.white,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      AppText.signInToYourAccount,
                                      style:
                                          _theme.textTheme.subtitle2?.copyWith(
                                        color: Colors.white70,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 20,
                                        vertical: 8,
                                      ),
                                      child: AppLoginFormField(
                                        icon: Icons.perm_identity,
                                        hintText: 'Username',
                                        autofillHints: const <String>[
                                          AutofillHints.email,
                                        ],
                                        onSave: form.saveUsername,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 20,
                                        vertical: 8,
                                      ),
                                      child: AppLoginFormField(
                                        icon: Icons.lock_outline,
                                        hintText: 'Password',
                                        autofillHints: const <String>[
                                          AutofillHints.password,
                                        ],
                                        obscureText: true,
                                        keyboardType:
                                            TextInputType.visiblePassword,
                                        onSave: form.savePassword,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 20,
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          _appAuthenticationAsyncValue.when(
                                              data: (appAuthentication) =>
                                                  TextButton(
                                                    onPressed: () => _submit(
                                                      context,
                                                      appAuthentication,
                                                    ),
                                                    style: TextButton.styleFrom(
                                                      elevation: 0,
                                                      primary: Colors.grey,
                                                      backgroundColor:
                                                          Colors.grey.shade100,
                                                      shape:
                                                          const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                          Radius.circular(6.0),
                                                        ),
                                                      ),
                                                    ),
                                                    child: const Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                        vertical: 12,
                                                        horizontal: 16,
                                                      ),
                                                      child: Text(
                                                        'LOGIN',
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                              loading: () =>
                                                  const CircularProgressIndicator(),
                                              error: (e, s) => const Text(
                                                  'Something went wrong')),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
