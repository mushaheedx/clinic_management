import 'package:clinic_management/common/strings.dart';
import 'package:clinic_management/widget/background.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/widget/form_field.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

class AccountsDateChange extends StatefulWidget {
  @override
  _AccountsDateChangeState createState() => _AccountsDateChangeState();
}

class _AccountsDateChangeState extends State<AccountsDateChange> {
  final formKey = GlobalKey<FormState>();

  Future<void> _submit(BuildContext context) async {
    final _formState = formKey.currentState;
    if (_formState?.validate() ?? false) {
      _formState!.save();
      Fimber.i('Form saved');
      // await screenController.update();
    }
  }

  TextEditingController? fromDataController;
  TextEditingController? toDataController;

  bool get isUpdateAvailable {
    return (fromDataController?.text.isNotEmpty ?? false) &&
        (toDataController?.text.isNotEmpty ?? false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    final _isSmall = _size.shortestSide < 900;

    final buttonBar = ButtonBar(
      children: [
        ElevatedButton(
          onPressed: _isLoading ? null : () => _submit(context),
          style: ElevatedButton.styleFrom(
            primary: AppTheme.primarySwatch,
            elevation: 0,
          ),
          child: const Padding(
            padding: EdgeInsets.symmetric(
              vertical: 8,
              horizontal: 8,
            ),
            child: Text('UPDATE'),
          ),
        ),
      ],
    );

    Widget child;
    if (_isSmall) {
      child = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AppDateFormField(
            hintText: 'dd/mm/yyyy',
            icon: Icons.calendar_today_outlined,
            label: 'Transaction From Date',
            useInvertedTheme: true,
            onValidate: (value, date) {
              //
            },
            onSave: (String? value, DateTime? date) {
              //
            },
          ),
          const SizedBox(height: 10),
          AppDateFormField(
            hintText: 'dd/mm/yyyy',
            icon: Icons.calendar_today_outlined,
            label: 'Transaction To Date',
            useInvertedTheme: true,
            onValidate: (value, date) {
              //
            },
            onSave: (String? value, DateTime? date) {
              //
            },
          ),
          const SizedBox(height: 12),
          buttonBar,
        ],
      );
    } else {
      child = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: AppDateFormField(
                    hintText: 'dd/mm/yyyy',
                    icon: Icons.calendar_today_outlined,
                    label: 'Transaction From Date',
                    useInvertedTheme: true,
                    onValidate: (value, date) {
                      //
                    },
                    onSave: (String? value, DateTime? date) {
                      //
                    },
                  ),
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: AppDateFormField(
                    hintText: 'dd/mm/yyyy',
                    icon: Icons.calendar_today_outlined,
                    label: 'Transaction To Date',
                    useInvertedTheme: true,
                    onValidate: (value, date) {
                      //
                    },
                    onSave: (String? value, DateTime? date) {
                      //
                    },
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 12),
          buttonBar,
        ],
      );
    }

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 12.0,
      ),
      child: Form(
        key: formKey,
        child: child,
      ),
    );
  }
}

class AccountsScreen extends StatelessWidget {
  const AccountsScreen();

  @override
  Widget build(BuildContext context) {
    return AppBackground(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: const Text('Accounts'),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 22,
              horizontal: 22,
            ),
            child: ConstrainedBox(
              constraints: const BoxConstraints(
                maxWidth: 900,
              ),
              child: Material(
                color: Colors.white38,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 22,
                    horizontal: 22,
                  ),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Flexible(
                            flex: 0,
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: 8.0,
                                horizontal: 8.0,
                              ),
                              child: Hero(
                                tag: HeroTags.accountsIcon,
                                child: Icon(
                                  Icons.assessment,
                                  size: 50,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      AccountsDateChange(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
