import 'package:clinic_management/di/visits.dart';
import 'package:clinic_management/plain/patient_record.dart';
import 'package:clinic_management/widget/else.dart';
import 'package:clinic_management/widget/on_error.dart';
import 'package:clinic_management/widget/unit_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PatientVisitHistory extends StatelessWidget {
  final PatientRecord record;

  const PatientVisitHistory({
    Key? key,
    required this.record,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(
          child: Consumer(
            builder: (context, reader, _) {
              final patientVisitHistoryAsync =
                  reader(patientVisitHistoryProvider(record));
              return patientVisitHistoryAsync.when(
                data: (data) {
                  if (data.data == null) {
                    return OnError(data.error, data.stackTrace as StackTrace?);
                  }
                  if (data.data?.isEmpty ?? true) {
                    return const Text('NO VISIT HISTORY');
                  }
                  final _data = data.data!;

                  return ListView.builder(
                    itemCount: _data.length,
                    shrinkWrap: true,
                    padding: const EdgeInsets.symmetric(
                      vertical: 40,
                    ),
                    itemBuilder: (context, index) {
                      final _visit = _data[index];

                      return ExpansionTile(
                        title: UnitInfo(
                          title: 'Complaint',
                          value: _visit.complaint ?? '',
                        ),
                        children: [
                          ListTile(
                            title: const Text('Complaint:'),
                            subtitle: Text(_visit.complaint ?? ''),
                          ),
                          ListTile(
                            title: const Text('Prescribed medicine:'),
                            subtitle: Text(_visit.medicine ?? ''),
                          ),
                          ListTile(
                            title: const Text('Treatment:'),
                            subtitle: Text(_visit.treatment ?? ''),
                          ),
                          ListTile(
                            title: const Text('Follow up details:'),
                            subtitle: Text(_visit.followUpDetails ?? ''),
                          ),
                          ListTile(
                            title: const Text('Time of visit:'),
                            subtitle: Text(_visit.time ?? ''),
                          ),
                          ListTile(
                            title: const Text('Date of last visit:'),
                            subtitle: Text(_visit.date ?? ''),
                          ),
                          ListTile(
                            title: const Text('Date of first visit:'),
                            subtitle: Text(_visit.dateVisit ?? ''),
                          ),
                          ListTile(
                            title: const Text('Reference:'),
                            subtitle: Text(_visit.reference ?? ''),
                          ),
                          ListTile(
                            title: const Text('Additional remarks:'),
                            subtitle: Text(_visit.additionalRemarks ?? ''),
                          ),
                          ListTile(
                            title: const Text('Fees:'),
                            subtitle: Text(_visit.fee ?? ''),
                          ),
                          ListTile(
                            title: const Text('Fee balance:'),
                            subtitle: Text(_visit.feeBalance ?? ''),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 8.0,
                              horizontal: 8.0,
                            ),
                            child: ButtonBar(
                              alignment: MainAxisAlignment.start,
                              children: [
                                TextButton(
                                  onPressed: () {},
                                  style: TextButton.styleFrom(
                                    primary: Colors.red,
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text('DELETE'),
                                  ),
                                ),
                                ElevatedButton.icon(
                                  onPressed: () {},
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.grey,
                                    elevation: 0,
                                  ),
                                  icon: const Padding(
                                    padding: EdgeInsetsDirectional.only(
                                      start: 8,
                                      top: 8,
                                      bottom: 8,
                                    ),
                                    child: Icon(Icons.edit),
                                  ),
                                  label: const Padding(
                                    padding: EdgeInsetsDirectional.only(
                                      end: 8,
                                      top: 8,
                                      bottom: 8,
                                    ),
                                    child: Text('EDIT'),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    },
                  );
                },
                loading: () => ElseWrap(
                  child: SizedBox.fromSize(
                    size: const Size.square(25),
                    child: const CircularProgressIndicator(),
                  ),
                ),
                error: (e, s) => ElseWrap(
                  child: OnError(e, s),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
