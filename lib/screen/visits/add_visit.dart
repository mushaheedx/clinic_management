import 'package:clinic_management/api/management_api.dart';
import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/di/visits.dart';
import 'package:clinic_management/plain/autocomplete_form.dart';
import 'package:clinic_management/utils/search/scoring_search.dart';
import 'package:clinic_management/utils/validation.dart';
import 'package:clinic_management/widget/addable_chips_form_field.dart';
import 'package:clinic_management/widget/form_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

const _height = SizedBox(
  height: 16,
);

class _VisitDetailsSuggestions {
  Future<Iterable<String?>?> _fetchUnusedMedicines(String query) async {
    if (query.isEmpty) return null;

    final _results = await ManagementApi.medicineAutocompleteSuggestions(
      MedicineAutocompleteSuggestions(query),
    );

    if (_results.data != null) {
      final _suggestions = _results.data!.suggestions;
      _suggestions.removeWhere((e) => e?.value?.contains('<a href') ?? false);
      return _suggestions.map<String?>((e) => e?.value);
    }
  }

  Future<Iterable<String?>?> _fetchUsedMedicines(String query) async {
    if (query.isEmpty) return null;

    final _results = await ManagementApi.medicines();

    if (_results.data?.isNotEmpty ?? true) {
      final _suggestions = _results.data!;
      _suggestions.removeWhere((e) => e.medicineName?.isEmpty ?? true);
      return _suggestions.map<String?>((e) => e.medicineName);
    }
  }

  Future<Iterable<String?>?> fetchSingleMedicineSuggestions(
    String query,
  ) async {
    final _results = await Future.wait([
      _fetchUnusedMedicines(query),
      _fetchUsedMedicines(query),
    ]);
    final List<String?> _result = [];
    for (final item in _results) {
      if (item != null) _result.addAll(item);
    }

    if (_results.isEmpty) return null;

    final _resultNonNull = <String>[];

    for (final it in _result) {
      if (it != null) {
        _resultNonNull.add(it);
      }
    }

    final _search = ScoringSearch();
    return _search.inStrings(_resultNonNull, query);
  }
}

class AddPatientVisitDetailsScreen extends StatefulWidget {
  const AddPatientVisitDetailsScreen();

  @override
  _AddPatientVisitDetailsScreenState createState() =>
      _AddPatientVisitDetailsScreenState();
}

class _AddPatientVisitDetailsScreenState
    extends State<AddPatientVisitDetailsScreen> {
  final addablePatientVisitDetails = getAddableDetailsOfPatientVisit();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final _medicinePrescriptionChipLabels =
      ChangeNotifierProvider<InputChipLabels>((ref) {
    return InputChipLabels();
  });

  static Future<Iterable<String?>?> _searchMedicines(String query) async {
    if (query.isEmpty) return null;

    final _suggestionsProvider = _VisitDetailsSuggestions();

    return _suggestionsProvider.fetchSingleMedicineSuggestions(query);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('VISIT'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 12,
        ),
        children: [
          Column(
            children: [
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 600),
                child: Form(
                  key: formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 25,
                      ),
                      AppDateFormField(
                        label: 'Date of visit',
                        labelColor: Colors.white,
                        icon: Icons.calendar_today_outlined,
                        hintText: 'dd-mm-yyyy',
                        initialDate: DateTime.now(),
                        onValidate: (value, dateTime) =>
                            cannotBeEmpty('Date of visit', value),
                        onSave: (value, dateTime) {
                          addablePatientVisitDetails.copy(
                            (data) => data.copyWith(dateVisit: value),
                          );
                        },
                      ),
                      _height,
                      AppTimeFormField(
                        label: 'Time',
                        labelColor: Colors.white,
                        icon: Icons.access_time,
                        hintText: 'hh:mm',
                        initialTime: TimeOfDay.now(),
                        onValidate: (value, dateTime) =>
                            cannotBeEmpty('Time', value),
                        onSave: (value, dateTime) {
                          addablePatientVisitDetails.copy(
                            (data) => data.copyWith(time: value),
                          );
                        },
                      ),
                      _height,
                      AppTextFormField(
                        maxLines: 3,
                        labelText: 'Complaint',
                        hintText: 'Complaint / Symptoms',
                        onValidate: (value) =>
                            cannotBeEmpty('Complaint', value),
                        onSave: (value) {
                          addablePatientVisitDetails
                              .copy((data) => data.copyWith(complaint: value));
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Treatment Given',
                        hintText: 'Select the treatment',
                        onValidate: (value) =>
                            cannotBeEmpty('Treatment Given', value),
                        onSave: (value) {
                          addablePatientVisitDetails
                              .copy((data) => data.copyWith(treatment: value));
                        },
                      ),
                      _height,
                      ChipsAddableAndSuggestibleField(
                        fetchSuggestions: _searchMedicines,
                        getChipLabels: (ScopedReader watch) {
                          return watch(_medicinePrescriptionChipLabels);
                        },
                        hintText: 'Medicine precriptions',
                        labelText: 'Prescribed Medicine',
                        onSave: (String? value) {
                          addablePatientVisitDetails
                              .copy((data) => data.copyWith(medicine: value));
                        },
                        onValidate: (value) {
                          final isEmpty = value?.trim().isEmpty ?? true;
                          if (isEmpty) return null;
                          return 'You must either clear or add prescriptions';
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Follow Up',
                        hintText: 'Follow up details',
                        onValidate: (value) =>
                            cannotBeEmpty('Follow Up', value),
                        onSave: (value) {
                          addablePatientVisitDetails.copy(
                              (data) => data.copyWith(followUpDetails: value));
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Reference',
                        hintText: 'Select/Provide a reference',
                        onValidate: (value) =>
                            cannotBeEmpty('Reference', value),
                        onSave: (value) {
                          addablePatientVisitDetails
                              .copy((data) => data.copyWith(reference: value));
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Fees',
                        hintText: 'Fees',
                        onValidate: (value) => cannotBeEmpty('Fees', value),
                        onSave: (value) {
                          addablePatientVisitDetails
                              .copy((data) => data.copyWith(fee: value));
                        },
                      ),
                      _height,
                      AppTextFormField(
                        labelText: 'Balance',
                        hintText: 'Fee Balance',
                        onValidate: (value) => cannotBeEmpty('Balance', value),
                        onSave: (value) {
                          addablePatientVisitDetails
                              .copy((data) => data.copyWith(feeBalance: value));
                        },
                      ),
                      _height,
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          final _formState = formKey.currentState;
                          if (_formState == null) {
                            Fimber.i('Form state null');
                            return;
                          }
                          if (_formState.validate()) {
                            _formState.save();
                            Fimber.i(
                                '${addablePatientVisitDetails.data.data.toJson()}');
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: AppTheme.primarySwatch,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 16.0,
                            horizontal: 16,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Text('ADD REPORT'),
                              Icon(
                                Icons.chevron_right_rounded,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
