import 'package:clinic_management/common/insets.dart';
import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/di/initialization.dart';
import 'package:clinic_management/utils/remove_loader/remove_loader.dart';
import 'package:clinic_management/widget/logo.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AppSplashScreen extends StatefulWidget {
  const AppSplashScreen();

  @override
  _AppSplashScreenState createState() => _AppSplashScreenState();
}

class _AppSplashScreenState extends State<AppSplashScreen> {
  @override
  void initState() {
    super.initState();
    _removeLoaderFromHtml();
    final appInitialization = context.read(appInitializationProvider);
    appInitialization.onInitialized(() {
      Fimber.i('Initialization completed');
    });
  }

  void _removeLoaderFromHtml() {
    if (kIsWeb) {
      removeLoaderFromHtml();
    }
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        gradient: AppDecoration.gradient,
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(
              maxWidth: 600,
            ),
            child: Padding(
              padding: AppInsets.all16,
              child: AppLogo(
                color: Colors.grey.shade900,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
