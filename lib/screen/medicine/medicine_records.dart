import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/delegate/medicine_record_search.dart';
import 'package:clinic_management/di/medicines.dart';
import 'package:clinic_management/di/patients.dart';
import 'package:clinic_management/widget/on_error.dart';
import 'package:clinic_management/widget/search_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MedicineRecordsScreen extends ConsumerWidget {
  const MedicineRecordsScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader reader) {
    final _data = reader(medicinesDataProvider);
    final _theme = Theme.of(context);
    final _textTheme = _theme.textTheme;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Medicine Records'),
        actions: [
          SearchAction(
            onPressed: () async {
              final _data = await context.read(medicinesDataProvider.future);
              final _records = _data.data ?? const [];
              await showSearch(
                context: context,
                delegate: MedicineRecordsSearchDelegate(_records),
              );
            },
          ),
          const SizedBox(
            width: 8,
          ),
          IconButton(
            onPressed: () {
              context.read(patientGridViewStatusProvider.notifier).toggle();
            },
            icon: Consumer(
              builder: (context, reader, _) {
                return Icon(
                  reader(patientGridViewStatusProvider)
                      ? Icons.grid_on_outlined
                      : Icons.grid_off_outlined,
                );
              },
            ),
          ),
          const SizedBox(
            width: 20,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 8.0,
          vertical: 16.0,
        ),
        child: Column(
          children: [
            Expanded(
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 8,
                    horizontal: 10,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        flex: 0,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Text(
                            'Medicines',
                            textAlign: TextAlign.start,
                            style: _textTheme.headline6,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 0,
                        child: DefaultTextStyle(
                          style: _textTheme.subtitle2!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 2.0),
                            child: Row(
                              children: const [
                                Expanded(child: Text('Name')),
                                Flexible(
                                  flex: 0,
                                  child: Text('Status'),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const Flexible(
                        flex: 0,
                        child: Divider(
                          thickness: 3,
                        ),
                      ),
                      _data.when(
                        data: (data) {
                          if (data.data?.isEmpty ?? true) {
                            return OnError(
                              data.error,
                              data.stackTrace as StackTrace?,
                            );
                          }

                          data.data?.sort();

                          return Flexible(
                            child: DefaultTextStyle(
                              style: _textTheme.bodyText2!.copyWith(
                                fontWeight: FontWeight.w300,
                              ),
                              child: ListView.separated(
                                shrinkWrap: true,
                                itemCount: data.data?.length ?? 0,
                                separatorBuilder: (context, index) {
                                  return Container(
                                    height: 1.0,
                                    width: double.infinity,
                                    color: DividerTheme.of(context).color ??
                                        Theme.of(context).dividerColor,
                                  );
                                },
                                itemBuilder: (context, index) {
                                  final _medicine = data.data?.elementAt(index);
                                  if (_medicine == null) {
                                    return AppDecoration.emptyBox;
                                  }
                                  final bool _isActive =
                                      _medicine.isActive == '1';

                                  return TextButton(
                                    onPressed: () {
                                      // Enable medicine
                                      // Disable medicine
                                    },
                                    style: TextButton.styleFrom(
                                      primary: Colors.white,
                                      padding: EdgeInsets.zero,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12.0),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: Text(
                                                  _medicine.medicineName ??
                                                      '')),
                                          Flexible(
                                            flex: 0,
                                            child:
                                                Text(_isActive ? 'Yes' : 'No'),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          );
                        },
                        loading: () {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox.fromSize(
                                size: const Size.square(25),
                                child: const CircularProgressIndicator(),
                              ),
                            ],
                          );
                        },
                        error: (e, t) => Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            OnError(e, t),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).pushNamed(
            AppRouteName.medicineRecordsAdd,
          );
        },
        icon: const Icon(Icons.medical_services_outlined),
        label: const Text('ADD MEDICINE'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
