import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/di/medicines.dart';
import 'package:clinic_management/utils/validation.dart';
import 'package:clinic_management/widget/form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

const _height = SizedBox(
  height: 16,
);

class AddMedicineScreen extends StatefulWidget {
  @override
  _AddMedicineScreenState createState() => _AddMedicineScreenState();
}

class _AddMedicineScreenState extends State<AddMedicineScreen> {
  final addableMedicineForm = getAddableMedicineForm();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextFormFieldTools medicineFieldTools = TextFormFieldTools();

  @override
  void initState() {
    medicineFieldTools.onInitState();
    super.initState();
  }

  @override
  void dispose() {
    medicineFieldTools.onDispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add a Medicine'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 12,
        ),
        children: [
          Column(
            children: [
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 600),
                child: Form(
                  key: formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 25,
                      ),
                      AppTextFormField(
                        labelText: 'Medicine Name',
                        hintText: 'Medicine',
                        onValidate: (value) =>
                            cannotBeEmpty('Medicine Name', value),
                        onSave: (value) {
                          addableMedicineForm.copy(
                              (data) => data.copyWith(medicineName: value));
                        },
                      ),
                      _height,
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          final _formState = formKey.currentState;
                          if (_formState == null) {
                            Fimber.i('Form state null');
                            return;
                          }
                          if (_formState.validate()) {
                            _formState.save();
                            Fimber.i(
                                '${addableMedicineForm.data.data.toJson()}');
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: AppTheme.primarySwatch,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 16.0,
                            horizontal: 16,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Text('ADD'),
                              Icon(
                                Icons.chevron_right_rounded,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
