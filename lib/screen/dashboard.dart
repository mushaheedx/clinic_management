import 'package:clinic_management/common/insets.dart';
import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/di/authentication.dart';
import 'package:clinic_management/di/medicines.dart';
import 'package:clinic_management/di/patients.dart';
import 'package:clinic_management/di/referers.dart';
import 'package:clinic_management/screen/accounts/accounts_card.dart';
import 'package:clinic_management/widget/background.dart';
import 'package:clinic_management/screen/medicine/medicine_card.dart';
import 'package:clinic_management/screen/patients/patient_card.dart';
import 'package:clinic_management/screen/referers/referers_card.dart';
import 'package:clinic_management/widget/search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;

    final _isSmall = _size.shortestSide < 900;

    return AppBackground(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: const Text('Clinic Management'),
          actions: [
            PopupMenuButton<String>(
              icon: const Icon(
                Icons.more_horiz_outlined,
              ),
              onSelected: (value) async {
                switch (value) {
                  case 'sign-out':
                    final appAuthentication =
                        await context.read(appAuthenticationProvider.future);
                    appAuthentication.logout();
                    Navigator.of(context).pushNamedAndRemoveUntil(
                      AppRouteName.login,
                      (route) => false,
                    );
                    break;
                  default:
                }
              },
              itemBuilder: (context) => [
                PopupMenuItem<String>(
                  value: 'sign-out',
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.logout,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text('Sign out'),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(
            horizontal: 8.0,
            vertical: 16.0,
          ),
          children: [
            const AppSearchBar(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: Container(
                    padding: AppInsets.all16,
                    constraints: const BoxConstraints(
                      maxWidth: 900,
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Text(
                                'Recents',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4!
                                    .copyWith(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            ],
                          ),
                        ),
                        if (_isSmall) const PatientsCard(),
                        if (_isSmall) const MedicalReferersCard(),
                        if (!_isSmall)
                          Row(
                            children: const [
                              Flexible(
                                child: PatientsCard(),
                              ),
                              Flexible(
                                child: MedicalReferersCard(),
                              )
                            ],
                          ),
                        if (_isSmall) const MedicinesCard(),
                        if (!_isSmall)
                          Row(
                            children: const [
                              Flexible(
                                child: MedicinesCard(),
                              ),
                              Flexible(
                                child: AppDecoration.emptyBox,
                              ),
                            ],
                          ),
                        const SizedBox(
                          height: 18,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Text(
                                'Other',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4!
                                    .copyWith(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            AccountsCard(),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            context.refresh(medicinesDataProvider);
            context.refresh(patientsDataProvider);
            context.refresh(referersDataProvider);
          },
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          icon: const Icon(Icons.refresh_outlined),
          label: const Text('Refresh'),
        ),
      ),
    );
  }
}
