import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/plain/patient_record.dart';
import 'package:flutter/material.dart';

class PatientRecordsSearchDelegate extends SearchDelegate {
  final List<PatientRecord> records;

  PatientRecordsSearchDelegate(this.records);

  @override
  List<Widget> buildActions(BuildContext context) {
    if (query.isEmpty) return const [];
    return [
      TextButton(
        onPressed: () {
          query = '';
        },
        child: const Text('CLEAR'),
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) => const CloseButton();

  @override
  Widget buildResults(BuildContext context) {
    if (query.isEmpty) return AppDecoration.emptyBox;
    final _children = <Widget>[];

    for (final element in records) {
      final _lowercaseq = query.toLowerCase();
      final _containsName =
          element.name?.toLowerCase().contains(_lowercaseq) ?? false;
      final _containsNumber = element.mobile?.contains(_lowercaseq) ?? false;
      final _containsOpdId = element.opdId?.contains(_lowercaseq) ?? false;
      if (_containsName || _containsNumber || _containsOpdId) {
        final _tile = TextButton(
          onPressed: () {
            Navigator.of(context).pushNamed(
              AppRouteName.patientInfo,
              arguments: element,
            );
          },
          style: TextButton.styleFrom(
            primary: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Flexible(
                  flex: 0,
                  child: SizedBox(
                    width: 65,
                    child: Text(element.opdId ?? ''),
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: Text(element.name ?? ''),
                ),
                const SizedBox(
                  width: 8,
                ),
                Flexible(
                  flex: 0,
                  child: Text(element.mobile ?? ''),
                )
              ],
            ),
          ),
        );
        _children.add(_tile);
      }
    }

    if (_children.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('No matching results found'),
          ],
        ),
      );
    }
    final _textTheme = Theme.of(context).textTheme;

    return DefaultTextStyle(
      style: _textTheme.bodyText2!.copyWith(
        fontWeight: FontWeight.bold,
      ),
      child: ListView.builder(
        itemCount: _children.length,
        itemBuilder: (context, index) {
          return _children[index];
        },
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text('Hit enter to show results'),
        ],
      ),
    );
  }
}
