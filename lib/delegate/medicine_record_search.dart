import 'package:clinic_management/common/styles.dart';
import 'package:clinic_management/plain/medicines.dart';
import 'package:flutter/material.dart';

class MedicineRecordsSearchDelegate extends SearchDelegate {
  final List<Medicine> records;

  MedicineRecordsSearchDelegate(this.records);

  @override
  List<Widget> buildActions(BuildContext context) {
    if (query.isEmpty) return const [];
    return [
      TextButton(
        onPressed: () {
          query = '';
        },
        child: const Text('CLEAR'),
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) => const CloseButton();

  @override
  Widget buildResults(BuildContext context) {
    if (query.isEmpty) return AppDecoration.emptyBox;
    final _children = <Widget>[];

    for (final element in records) {
      final _lowercaseq = query.toLowerCase();
      final _containsName =
          element.medicineName?.toLowerCase().contains(_lowercaseq) ?? false;
      if (_containsName) {
        final _tile = TextButton(
          onPressed: () {
            // TOggle use
          },
          style: TextButton.styleFrom(
            primary: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(element.medicineName ?? ''),
          ),
        );
        _children.add(_tile);
      }
    }

    if (_children.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('No matching results found'),
          ],
        ),
      );
    }
    final _textTheme = Theme.of(context).textTheme;

    return DefaultTextStyle(
      style: _textTheme.bodyText2!.copyWith(
        fontWeight: FontWeight.bold,
      ),
      child: ListView.builder(
        itemCount: _children.length,
        itemBuilder: (context, index) {
          return _children[index];
        },
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text('Hit enter to show results'),
        ],
      ),
    );
  }
}
