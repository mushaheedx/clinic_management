import 'package:clinic_management/utils/copyable_details.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

typedef DetailsSubmissionCallback<DETAILS_TYPE, RESPONSE> = Future<RESPONSE>
    Function(
  DETAILS_TYPE details,
);

class AddableDetails<DETAILS_TYPE, RESPONSE> {
  final CopyableDetails<DETAILS_TYPE> data;
  final DetailsSubmissionCallback<DETAILS_TYPE, RESPONSE>
      _detailsSubmissionCallback;

  AddableDetails(DETAILS_TYPE _data, this._detailsSubmissionCallback)
      : data = CopyableDetails<DETAILS_TYPE>(_data);

  void copy(CopyWithCallback<DETAILS_TYPE> copyWithAction) =>
      data.copyWith(copyWithAction);

  FutureProvider<RESPONSE> submit() =>
      FutureProvider<RESPONSE>((ProviderReference ref) {
        return _detailsSubmissionCallback(data.data);
      });
}
