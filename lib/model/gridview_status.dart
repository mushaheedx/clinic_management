import 'package:flutter_riverpod/flutter_riverpod.dart';

class GridViewStatus extends StateNotifier<bool> {
  GridViewStatus() : super(false);

  bool get isEnabled => state;

  void toggle([bool? value]) {
    if (value != null) {
      state = value;
    } else {
      state = !state;
    }
  }
}
