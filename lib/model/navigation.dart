import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/plain/patient_record.dart';
import 'package:clinic_management/screen/accounts/accounts.dart';
import 'package:clinic_management/screen/dashboard.dart';
import 'package:clinic_management/screen/login.dart';
import 'package:clinic_management/screen/medicine/add_medicine.dart';
import 'package:clinic_management/screen/medicine/medicine_records.dart';
import 'package:clinic_management/screen/patients/add_patient.dart';
import 'package:clinic_management/screen/patients/patient_info.dart';
import 'package:clinic_management/screen/patients/patients.dart';
import 'package:clinic_management/screen/referers/add_referers.dart';
import 'package:clinic_management/screen/referers/referer_records.dart';
import 'package:clinic_management/screen/splash.dart';
import 'package:clinic_management/screen/visits/add_visit.dart';
import 'package:flutter/material.dart';

class AppNavigation {
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Widget _builder(BuildContext context, RouteSettings settings) {
    switch (settings.name) {
      case AppRouteName.login:
        return LoginScreen();
      case AppRouteName.dashboard:
        return const DashboardScreen();
      case AppRouteName.patientRecords:
        return const PatientRecordsScreen();
      case AppRouteName.patientRecordsAdd:
        return const AddPatientScreen();
      case AppRouteName.patientInfo:
        // ignore: cast_nullable_to_non_nullable
        final PatientRecord record = settings.arguments as PatientRecord;
        return PatientInfoScreen(
          record: record,
        );
      case AppRouteName.patientAddVisit:
        return const AddPatientVisitDetailsScreen();
      case AppRouteName.medicineRecords:
        return const MedicineRecordsScreen();
      case AppRouteName.medicineRecordsAdd:
        return AddMedicineScreen();
      case AppRouteName.medicalRefererRecords:
        return const MedicalRefererRecordsScreen();
      case AppRouteName.medicalRefererRecordsAdd:
        return const AddReferenceScreen();
      case AppRouteName.accountsScreen:
        return const AccountsScreen();
      case AppRouteName.root:
      default:
        return const AppSplashScreen();
    }
  }

  Route<dynamic> generateRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => _builder(context, settings),
      settings: settings,
    );
  }
}
