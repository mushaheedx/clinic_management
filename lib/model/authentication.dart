import 'package:clinic_management/api/management_api.dart';
import 'package:clinic_management/plain/login.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:shared_preferences/shared_preferences.dart';

class _AppAuthenticationKeys {
  _AppAuthenticationKeys._();

  static const authenticationId = 'authentication_id';
}

class AppAuthentication {
  AppAuthentication(this.preferences);

  final SharedPreferences preferences;

  bool get isLoggedIn => authenticationId.isNotEmpty;

  String get authenticationId {
    return preferences.getString(_AppAuthenticationKeys.authenticationId) ?? '';
  }

  bool get hasLoginExpired {
    return authenticationId.isEmpty;
  }

  Future<String?> login(
    final LoginForm form,
  ) async {
    final _response = await ManagementApi.login(
      form.username,
      form.password,
    );

    final _status = _response.data?.isSuccess ?? false;

    if (_status) {
      await preferences.setString(
        _AppAuthenticationKeys.authenticationId,
        _response.data?.response ?? '',
      );

      Fimber.i('[AppAuthentication] Login details saved.');

      return null;
    }

    _response.printErrorWithStackTrace();

    return _response.data?.response;
  }

  Future<bool> logout() async {
    final _noAuthId = await preferences.remove(
      _AppAuthenticationKeys.authenticationId,
    );
    return _noAuthId;
  }
}
