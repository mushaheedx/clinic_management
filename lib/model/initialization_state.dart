import 'dart:async';

/// Represent app's initialization state.
abstract class AppInitializationState {
  bool get isLoading;

  Object? get error;

  StackTrace? get stackTrace;

  Future<bool> isInitialized();

  Future<void> onInitialized(void Function() callback) async {
    await isInitialized();
    callback();
  }
}
