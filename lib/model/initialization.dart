import 'dart:async';

import 'package:clinic_management/common/route_names.dart';
import 'package:clinic_management/di/authentication.dart';
import 'package:clinic_management/model/authentication.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'initialization_state.dart';
import 'navigation.dart';

class _AppInitializationStateImpl extends AppInitializationState {
  bool _isLoading = false;

  @override
  bool get isLoading => _isLoading;

  Object? _error;

  @override
  Object? get error => _error;

  StackTrace? _stackTrace;

  @override
  StackTrace? get stackTrace => _stackTrace;

  final Completer<bool> _initializationCompleter = Completer<bool>();

  void onInitializationStart() {
    _isLoading = true;
  }

  void onInitializationComplete() {
    _isLoading = false;
    _initializationCompleter.complete(true);
  }

  void onInitializationError(Object error, [StackTrace? stackTrace]) {
    _error = error;
    _stackTrace = stackTrace;
    _initializationCompleter.completeError(error, stackTrace);
  }

  @override
  Future<bool> isInitialized() => _initializationCompleter.future;
}

class AppInitialization extends StateNotifier<AppInitializationState> {
  late AppAuthentication appAuthentication;

  final AppNavigation appNavigation;

  late SharedPreferences? _preferences;

  final ProviderReference ref;

  SharedPreferences get preferences => _preferences!;

  AppInitialization(this.ref, this.appNavigation)
      : super(_AppInitializationStateImpl()) {
    _initialize();
  }

  void notify(void Function(_AppInitializationStateImpl) beforeNotify) {
    beforeNotify(state as _AppInitializationStateImpl);

    state = state;
  }

  Future<void> _initialize() async {
    notify((mstate) {
      mstate.onInitializationStart();
    });

    appAuthentication = await ref.read(appAuthenticationProvider.future);

    _onInitializationData(appAuthentication);
  }

  Future<void> _onInitializationData(
    AppAuthentication appAuthentication,
  ) async {
    _preferences = appAuthentication.preferences;
    try {
      final _delay = Future.delayed(
        const Duration(
          milliseconds: 300,
        ),
      );

      if (appAuthentication.hasLoginExpired) {
        appAuthentication.logout();
      }

      await _delay;

      notify((mstate) {
        mstate.onInitializationComplete();
      });

      if (appAuthentication.isLoggedIn) {
        appNavigation.navigatorKey.currentState
            ?.pushNamedAndRemoveUntil(AppRouteName.dashboard, (route) => false);
      } else {
        appNavigation.navigatorKey.currentState
            ?.pushNamedAndRemoveUntil(AppRouteName.login, (route) => false);
      }
    } catch (e, t) {
      notify((mstate) {
        mstate.onInitializationError(e, t);
      });

      Fimber.e('Exception caught during initialization', ex: e, stacktrace: t);
      appNavigation.navigatorKey.currentState
          ?.pushNamedAndRemoveUntil(AppRouteName.login, (route) => false);
    }
  }
}
